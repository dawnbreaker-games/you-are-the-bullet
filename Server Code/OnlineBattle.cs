using PlayerIO.GameLibrary;

[RoomType("Online Battle")]
public class OnlineBattle : Game<Player>
{
	GameMode gameMode;

	public override void GameStarted ()
	{
		gameMode = new GameMode(this);
	}

	public override void UserJoined (Player player)
	{
		gameMode.UserJoined (player);
	}

	public override void UserLeft (Player player)
	{
		gameMode.UserLeft (player);
	}

	public override void GotMessage (Player player, Message message)
	{
		gameMode.GotMessage (player, message);
	}
}