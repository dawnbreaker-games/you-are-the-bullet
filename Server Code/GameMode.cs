using System;
using PlayerIO.GameLibrary;
using System.Collections.Generic;

public class GameMode
{
	Game<Player> game;
	Dictionary<int, Message> spawnPlayerMessagesDict = new Dictionary<int, Message>();
	Random random = new Random();
	float mapRadius;
	float mapArea;
	const uint MIN_SPAWN_DISTANCE_FROM_PLAYERS = 25;
	const uint MAX_BODY_AND_WEAPON_SEPERATION = 15;
	const uint BODY_RADIUS = 2;
	const float WEAPON_RADIUS = 1.8f;
	const uint AREA_PER_PLAYER = 15708;

	public GameMode (Game<Player> game)
	{
		this.game = game;
	}

	public void UserJoined (Player player)
	{
		mapArea += AREA_PER_PLAYER;
		mapRadius = (float) Math.Sqrt(mapArea / Math.PI);
		foreach (KeyValuePair<int, Message> keyValuePair in spawnPlayerMessagesDict)
			player.Send(keyValuePair.Value);
		float minDistanceFromWall = MAX_BODY_AND_WEAPON_SEPERATION / 2 + BODY_RADIUS;
		Vector2 position = GetSpawnPoint(minDistanceFromWall, minDistanceFromWall + MIN_SPAWN_DISTANCE_FROM_PLAYERS);
		Message spawnPlayerMessage = Message.Create("Spawn Player", player.Id);
		foreach (Player player2 in game.Players)
			player2.Send(spawnPlayerMessage);
		spawnPlayerMessagesDict.Add(player.Id, spawnPlayerMessage);
	}

	public void UserLeft (Player player)
	{
		mapArea -= AREA_PER_PLAYER;
		mapRadius = (float) Math.Sqrt(mapArea / Math.PI);
		foreach (Player player2 in game.Players)
		{
			if (player != player2)
			{
				Message removePlayerMessage = Message.Create("Remove Player", player.Id);
				player2.Send(removePlayerMessage);
			}
		}
	}

	public void GotMessage (Player player, Message message)
	{
		if (message.Type == "Move Player")
		{
			bool isBody = message.GetBoolean(1);
			Vector2 position = new Vector2(message.GetFloat(1), message.GetFloat(2));
			Message movePlayerMessage = Message.Create("Move Player", player.Id, isBody, position.x, position.y);
			foreach (Player player2 in game.Players)
			{
				if (player != player2)
					player2.Send(movePlayerMessage);
			}
		}
		else
		{
			foreach (Player player2 in game.Players)
			{
				if (player != player2)
					player2.Send(message);
			}
		}
	}
	
	Vector2 GetSpawnPoint (float minDistanceFromWall, float minMetersFromPlayers)
	{
		while (true)
		{
			Vector2 spawnPosition = Vector2.Random(random) * MathExtensions.Random(0, mapRadius - minDistanceFromWall, random);
			bool isValidSpawnPosition = true;
			foreach (Player player in game.Players)
			{
				if ((spawnPosition - player.position).SqrMagnitude < minMetersFromPlayers * minMetersFromPlayers)
				{
					isValidSpawnPosition = false;
					break;
				}
			}
			if (isValidSpawnPosition)
				return spawnPosition;
		}
	}
}