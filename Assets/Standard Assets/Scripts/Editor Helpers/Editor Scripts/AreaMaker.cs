// #if UNITY_EDITOR
// using UnityEditor;
// using UnityEngine;
// using System.Collections;
// using System.Collections.Generic;

// namespace YouAreTheBullet
// {
// 	public class AreaMaker : SingletonEditorScript<AreaMaker>
// 	{
// 		public AreaPath[] areaPaths;

// 		[MenuItem("RPG/Make Area")]
// 		public static void _MakeArea ()
// 		{
// 			Instance.MakeArea ();
// 		}

// 		public virtual void MakeArea ()
// 		{
// 			StopAllCoroutines();
// 			foreach (AreaPath areaPath in areaPaths)
// 				areaPath.StopAllCoroutines();
// 			StartCoroutine(MakeAreaRoutine ());
// 		}

// 		public virtual IEnumerator MakeAreaRoutine ()
// 		{
// 			foreach (AreaPath areaPath in areaPaths)
// 				yield return StartCoroutine(areaPath.MakeRoutine ());
// 		}

// 		[MenuItem("RPG/Stop Making Area")]
// 		public static void _StopMakingArea ()
// 		{
// 			Instance.StopMakingArea ();
// 		}

// 		public virtual void StopMakingArea ()
// 		{
// 			StopAllCoroutines();
// 			foreach (AreaPath areaPath in areaPaths)
// 				areaPath.StopAllCoroutines();
// 		}
// 	}
// }
// #endif