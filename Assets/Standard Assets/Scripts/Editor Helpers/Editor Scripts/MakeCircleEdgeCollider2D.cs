#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace YouAreTheBullet
{
	[DisallowMultipleComponent]
	// [RequireComponent(typeof(EdgeCollider2D))]
	public class MakeCircleEdgeCollider2D : EditorScript
	{
		public Transform trs;
		public EdgeCollider2D edgeCollider2D;
		public float radius;
		[Range(4, 50)]
		public int pointCount;
		Vector2[] points;

		public override void Do ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			if (edgeCollider2D == null)
				edgeCollider2D = GetComponent<EdgeCollider2D>();
			points = new Vector2[pointCount];
			for (int i = 0; i < pointCount; i ++)
				points[i] = VectorExtensions.FromFacingAngle(((i + 1) * 360f / (pointCount - 1)) * (1f / 360f) * 360) * radius;
			edgeCollider2D.points = points;
		}
	}
}
#else
namespace YouAreTheBullet
{
	public class MakeCircleEdgeCollider2D : EditorScript
	{
	}
}
#endif