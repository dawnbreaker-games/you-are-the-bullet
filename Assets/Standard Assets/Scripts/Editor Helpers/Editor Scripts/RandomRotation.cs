﻿#if UNITY_EDITOR
using UnityEngine;
using System.Collections;

namespace YouAreTheBullet
{
	public class RandomRotation : EditorScript
	{
		public Transform trs;

		public override void Do ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			trs.eulerAngles = Vector3.forward * Random.value * 360;
		}
	}
}
#else
namespace YouAreTheBullet
{
	public class RandomRotation : EditorScript
	{
	}
}
#endif
