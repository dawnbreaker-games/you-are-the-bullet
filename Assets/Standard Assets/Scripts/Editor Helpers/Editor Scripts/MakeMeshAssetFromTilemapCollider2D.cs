﻿#if UNITY_EDITOR
using UnityEngine;
using UnityEngine.Tilemaps;

namespace YouAreTheBullet
{
	public class MakeMeshAssetFromTilemapCollider2D : MakeMeshAsset
	{
		public TilemapCollider2D tilemapCollider2D;

		public override void Do ()
		{
			if (tilemapCollider2D == null)
				tilemapCollider2D = GetComponent<TilemapCollider2D>();
			if (useSharedMesh)
				meshFilter.sharedMesh = tilemapCollider2D.CreateMesh(false, false);
			else
				meshFilter.mesh = tilemapCollider2D.CreateMesh(false, false);
			base.Do ();
		}
	}
}
#else
namespace YouAreTheBullet
{
	public class MakeMeshAssetFromTilemapCollider2D : EditorScript
	{
	}
}
#endif
