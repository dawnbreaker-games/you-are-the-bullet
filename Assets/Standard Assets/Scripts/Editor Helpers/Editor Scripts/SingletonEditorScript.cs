#if UNITY_EDITOR
using UnityEngine;

namespace YouAreTheBullet
{
	public class SingletonEditorScript<T> : EditorScript where T : MonoBehaviour
	{
		public static T instance;
		public static T Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<T>();
				return instance;
			}
		}
	}
}
#endif