#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using System.Collections;

namespace YouAreTheBullet
{
	[DisallowMultipleComponent]
	// [RequireComponent(typeof(PolygonCollider2D))]
	public class MakeCirclePolygonCollider2D : EditorScript
	{
		public Transform trs;
		public PolygonCollider2D polygonCollider2D;
		public float radius;
		[Range(3, 50)]
		public int pointCount;
		Vector2[] points;

		public override void Do ()
		{
			if (trs == null)
				trs = GetComponent<Transform>();
			if (polygonCollider2D == null)
				polygonCollider2D = GetComponent<PolygonCollider2D>();
			points = new Vector2[pointCount];
			for (int i = 0; i < pointCount; i ++)
				points[i] = VectorExtensions.FromFacingAngle((i * 360f / pointCount) * (1f / 360f) * 360) * radius;
			polygonCollider2D.points = points;
		}
	}
}
#else
namespace YouAreTheBullet
{
	public class MakeCirclePolygonCollider2D : EditorScript
	{
	}
}
#endif