﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	public class Bullet : Hazard
	{
		public float range;
		public float duration;
		public Rigidbody2D rigid;
		public float moveSpeed;
		public AutoDespawnMode autoDespawnMode;
		public ObjectPool.RangedDespawn rangedDespawn;
		public ObjectPool.DelayedDespawn delayedDespawn;
		public new Collider2D collider;
		public uint hitsTillDespawn;
		public LayerMask whatReducesHits;
		[HideInInspector]
		public bool dead;
		public float radius;
		public static List<Bullet> instances = new List<Bullet>();
		uint hitsTillDespawnRemaining;
		
		public virtual void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (rigid == null)
					rigid = GetComponent<Rigidbody2D>();
				return;
			}
#endif
			dead = false;
			hitsTillDespawnRemaining = hitsTillDespawn;
			if (autoDespawnMode == AutoDespawnMode.RangedAutoDespawn)
				rangedDespawn = ObjectPool.instance.RangeDespawn(prefabIndex, gameObject, trs, range);
			else if (autoDespawnMode == AutoDespawnMode.DelayedAutoDespawn)
				delayedDespawn = ObjectPool.instance.DelayDespawn(prefabIndex, gameObject, trs, duration);
			rigid.velocity = trs.up * moveSpeed;
			instances.Add(this);
		}

		public virtual void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			StopAllCoroutines();
			if (ObjectPool.instance != null)
			{
				if (autoDespawnMode == AutoDespawnMode.RangedAutoDespawn)
					ObjectPool.instance.CancelRangedDespawn (rangedDespawn);
				else if (autoDespawnMode == AutoDespawnMode.DelayedAutoDespawn)
					ObjectPool.instance.CancelDelayedDespawn (delayedDespawn);
			}
			dead = true;
			instances.Remove(this);
		}

		public override void OnCollisionEnter2D (Collision2D coll)
		{
			if (!dead)
			{
				base.OnCollisionEnter2D (coll);
				if (whatReducesHits.ContainsLayer(coll.gameObject.layer))
				{
					hitsTillDespawnRemaining --;
					if (hitsTillDespawnRemaining == 0)
						ObjectPool.instance.Despawn (prefabIndex, gameObject, trs);
				}
			}
		}

		public void Move (Vector2 move)
		{
			move = Vector2.ClampMagnitude(move, 1);
			rigid.velocity = move * moveSpeed;
		}

		public enum AutoDespawnMode
		{
			DontAutoDespawn,
			RangedAutoDespawn,
			DelayedAutoDespawn
		}
	}
}