using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.InputSystem;

namespace YouAreTheBullet
{
	public class Enemy : Entity
	{
		[Header("Enemy")]
		public float bodyDecayDuration;
		public Collider2D collider;
		public Transform faceTowardsPlayerTrs;
		public float stopDistance;
		public Transform deadBodyTrs;
		public float difficulty;
		public float radius;

		public override void HandleMovement ()
		{
			Vector2 toPlayer = Player.Instance.trs.position - trs.position;
			if (toPlayer.sqrMagnitude < stopDistance * stopDistance)
				Move (-toPlayer.normalized);
			else
				Move (toPlayer.normalized);
			faceTowardsPlayerTrs.up = toPlayer;
		}
		
		public override void Death ()
		{
			base.Death ();
			deadBodyTrs.gameObject.SetActive(true);
			deadBodyTrs.SetParent(null);
			Destroy(gameObject);
			Destroy(deadBodyTrs.gameObject, bodyDecayDuration);
		}
	}
}