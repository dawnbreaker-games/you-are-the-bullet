using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace YouAreTheBullet
{
	public class Player : Entity
	{
		[Header("Player")]
		public float damageOnHit;
		public static Player instance;
		public static Player Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<Player>();
				return instance;
			}
		}
		public static Vector2 StartPosition
		{
			get
			{
				return SaveAndLoadManager.GetVector2("Start position");
			}
			set
			{
				SaveAndLoadManager.SetVector2 ("Start position", value);
			}
		}

		public override void HandleMovement ()
		{
			if (InputManager.UsingTouchscreen)
			{
				Vector2 move = InputManager.instance.moveVirtualJoystick.touchPosition - InputManager.instance.moveVirtualJoystick.outerCircle.center;
				if (move.sqrMagnitude >= InputManager.instance.settings.defaultDeadzoneMin * InputManager.instance.settings.defaultDeadzoneMin)
					Move (move.normalized);
				else
					Move (Vector2.zero);
			}
			else if (InputManager.UsingGamepad)
			{
				Vector2 move = Gamepad.current.leftStick.ReadValue();
				if (move.sqrMagnitude >= InputManager.instance.settings.defaultDeadzoneMin * InputManager.instance.settings.defaultDeadzoneMin)
					Move (move.normalized);
				else
					Move (Vector2.zero);
			}
			else
			{
				if (Mouse.current.leftButton.isPressed)
					Move (((Vector2) GameCamera.Instance.camera.ScreenToWorldPoint(Mouse.current.position.ReadValue()) - (Vector2) trs.position).normalized);
				else
					Move (Vector2.zero);
			}
		}

		public override void Move (Vector2 move)
		{
			base.Move (move);
			if (move != Vector2.zero)
				trs.up = move;
		}
		
		public override void Death ()
		{
			base.Death ();
			SceneManager.LoadScene(SceneManager.GetActiveScene().name);
		}

		void OnCollisionEnter2D (Collision2D coll)
		{
			Enemy enemy = coll.gameObject.GetComponent<Enemy>();
			if (enemy != null)
				enemy.TakeDamage (damageOnHit);
		}
	}
}