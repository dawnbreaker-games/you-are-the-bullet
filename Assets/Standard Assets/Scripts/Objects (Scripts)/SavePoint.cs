﻿using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	[ExecuteInEditMode]
	public class SavePoint : MonoBehaviour
	{
		public bool Found
		{
			get
			{
				return SaveAndLoadManager.GetBool(name + " found", false);
			}
			set
			{
				SaveAndLoadManager.SetBool (name + " found", value);
			}
		}
		public static List<SavePoint> instances = new List<SavePoint>();
		public GameObject foundIndicator;
		public WorldMapIcon worldMapIcon;
		bool previousWorldMapInput;

		void Awake ()
		{
			instances.Add(this);
		}

		void OnDestroy ()
		{
			instances.Remove(this);
		}

		void OnTriggerEnter2D (Collider2D other)
		{
			if (other.gameObject == Player.instance.gameObject)
			{
				WorldMap.playerIsAtSavePoint = true;
				WorldMap.ExploredCellPositions = new Vector2Int[WorldMap.exploredCellPositions.Count];
				WorldMap.exploredCellPositions.CopyTo(WorldMap.ExploredCellPositions);
				Found = true;
				Player.StartPosition = Player.instance.trs.position;
			}
		}

		void OnTriggerExit2D (Collider2D other)
		{
			if (other.gameObject.layer != LayerMask.NameToLayer("Map"))
				WorldMap.playerIsAtSavePoint = false;
		}
	}
}