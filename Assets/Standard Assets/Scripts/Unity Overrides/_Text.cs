﻿using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	//[ExecuteAlways]
	[RequireComponent(typeof(Text))]
	[DisallowMultipleComponent]
	public class _Text : MonoBehaviour
	{
		[HideInInspector]
		public string defaultText;
		public bool useSeperateTextForInputDevices;
		public SerializableDictionary<InputDeviceGroup, MultilineString> inputDevicesDict = new SerializableDictionary<InputDeviceGroup, MultilineString>();
		public Text text;
		public static _Text[] instances = new _Text[0];

		void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (text == null)
					text = GetComponent<Text>();
				return;
			}
#endif
			inputDevicesDict.Init ();
			UpdateText ();
			instances = instances.Add(this);
		}

#if UNITY_EDITOR
		void OnValidate ()
		{
			defaultText = text.text;
		}
#endif
		
		public void UpdateText ()
		{
			MultilineString multilineString;
			if (useSeperateTextForInputDevices && inputDevicesDict.TryGetValue(new InputDeviceGroup(InputManager.inputDevices), out multilineString))
				text.text = multilineString.value;
			else
				text.text = defaultText;
		}

		void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			instances = instances.Remove(this);
		}

		[Serializable]
		public struct InputDeviceGroup
		{
			public InputDevice[] inputDevices;

			public InputDeviceGroup (InputDevice[] inputDevices)
			{
				this.inputDevices = inputDevices;
			}
		}
	}
}
