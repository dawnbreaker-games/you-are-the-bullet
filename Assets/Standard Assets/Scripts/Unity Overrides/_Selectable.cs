﻿using YouAreTheBullet;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;

//[ExecuteAlways]
public class _Selectable : UpdateWhileEnabled
{
	public Canvas canvas;
	public RectTransform canvasRectTrs;
#if UNITY_EDITOR
	public bool updateCanvas = true;
#endif
	public RectTransform rectTrs;
	public Selectable selectable;
	public float priority;
	public bool Interactable
	{
		get
		{
			return selectable.interactable;
		}
		set
		{
			if (value != selectable.interactable)
			{
				if (value)
					OnEnable ();
				else
					OnDisable ();
			}
			selectable.interactable = value;
		}
	}
	
	public virtual void UpdateCanvas ()
	{
		canvas = GetComponent<Canvas>();
		canvasRectTrs = GetComponent<RectTransform>();
		while (canvas == null)
		{
			canvasRectTrs = canvasRectTrs.parent.GetComponent<RectTransform>();
			canvas = canvasRectTrs.GetComponent<Canvas>();
		}
	}
	
	public override void OnEnable ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
		{
			if (rectTrs == null)
				rectTrs = GetComponent<RectTransform>();
			if (selectable == null)
				selectable = GetComponent<Selectable>();
			return;
		}
#endif
		UIControlManager.Instance.AddSelectable (this);
		UpdateCanvas ();
		base.OnEnable ();
	}
	
	public override void OnDisable ()
	{
#if UNITY_EDITOR
		if (!Application.isPlaying)
			return;
#endif
		UIControlManager.Instance.RemoveSelectable (this);
		base.OnDisable ();
	}
	
#if UNITY_EDITOR
	public override void DoUpdate ()
	{
		if (Application.isPlaying)
			return;
		if (updateCanvas)
		{
			updateCanvas = false;
			UpdateCanvas ();
		}
	}
#endif
}
