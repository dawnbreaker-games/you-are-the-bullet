﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	public class PlayerPrefsSlider : _Slider
	{
		public string playerPrefsKey;
		
		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			slider.value = SaveAndLoadManager.GetFloat(playerPrefsKey, slider.value);
			base.Awake ();
		}
		
		public override void DoUpdate ()
		{
			base.DoUpdate ();
			SaveAndLoadManager.SetFloat(playerPrefsKey, slider.value);
		}
	}
}
