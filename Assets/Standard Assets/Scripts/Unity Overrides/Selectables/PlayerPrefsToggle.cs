﻿using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	[RequireComponent(typeof(Toggle))]
	public class PlayerPrefsToggle : _Selectable
	{
		public Toggle toggle;
		public string playerPrefsKey;
		
		public virtual void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			toggle.isOn = SaveAndLoadManager.GetInt(playerPrefsKey, toggle.isOn.GetHashCode()) == 1;
		}

		public override void DoUpdate ()
		{
#if UNITY_EDITOR
			base.DoUpdate ();
#endif
			SaveAndLoadManager.SetInt(playerPrefsKey, toggle.isOn.GetHashCode());
		}
	}
}
