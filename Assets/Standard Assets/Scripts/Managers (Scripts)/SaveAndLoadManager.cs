﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	public class SaveAndLoadManager : SingletonMonoBehaviour<SaveAndLoadManager>
	{
		public static bool GetBool (string key, bool value = false)
		{
			return PlayerPrefs.GetInt(key, value.GetHashCode()) == 1;
		}

		public static void SetBool (string key, bool value)
		{
			PlayerPrefs.SetInt(key, value.GetHashCode());
		}

		public static int GetInt (string key, int value = 0)
		{
			return PlayerPrefs.GetInt(key, value);
		}

		public static void SetInt (string key, int value)
		{
			PlayerPrefs.SetInt(key, value);
		}

		public static float GetFloat (string key, float value = 0)
		{
			return PlayerPrefs.GetFloat(key, value);
		}

		public static void SetFloat (string key, float value)
		{
			PlayerPrefs.SetFloat(key, value);
		}

		public static Vector2 GetVector2 (string key, Vector2 value = new Vector2())
		{
			return new Vector2(PlayerPrefs.GetFloat(key + ".x", value.x), PlayerPrefs.GetFloat(key + ".y", value.y));
		}

		public static void SetVector2 (string key, Vector2 value)
		{
			PlayerPrefs.SetFloat(key + ".x", value.x);
			PlayerPrefs.SetFloat(key + ".y", value.y);
		}

		public static Vector2Int[] GetVector2IntArray (string key, Vector2Int[] values = null)
		{
			List<Vector2Int> output = new List<Vector2Int>();
			int index = 0;
			while (PlayerPrefs.HasKey(key + "[" + index + "].x"))
			{
				output.Add(new Vector2Int(PlayerPrefs.GetInt(key + "[" + index + "].x"), PlayerPrefs.GetInt(key + "[" + index + "].y")));
				index ++;
			}
			if (output.Count == 0)
				return values;
			else
				return output.ToArray();
		}

		public static void SetVector2IntArray (string key, Vector2Int[] values)
		{
			for (int i = 0; i < values.Length; i ++)
			{
				Vector2Int value = values[i];
				PlayerPrefs.SetInt(key + "[" + i + "].x", value.x);
				PlayerPrefs.SetInt(key + "[" + i + "].y", value.y);
			}
			int index = values.Length;
			while (PlayerPrefs.HasKey(key + "[" + index + "].x"))
			{
				PlayerPrefs.DeleteKey(key + "[" + index + "].x");
				PlayerPrefs.DeleteKey(key + "[" + index + "].y");
				index ++;
			}
		}

		public static string GetString (string key, string value = "")
		{
			return PlayerPrefs.GetString(key, value);
		}

		public static void SetString (string key, string value)
		{
			PlayerPrefs.SetString(key, value);
		}

		public static void DeleteKey (string key)
		{
			PlayerPrefs.DeleteKey(key);
		}

		public static void DeleteAll (string key)
		{
			PlayerPrefs.DeleteAll();
		}
	}
}