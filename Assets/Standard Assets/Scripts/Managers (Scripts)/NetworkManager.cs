using System;
using Extensions;
using UnityEngine;
// using PlayerIOClient;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	public class NetworkManager : SingletonMonoBehaviour<NetworkManager>
	{
		// public static Connection connection;
		// public static Client client;
		public TemporaryTextObject notificationTextObject;
		public static bool IsOnline
		{
			get
			{
				return SaveAndLoadManager.GetBool("Is online", false);
			}
			set
			{
				SaveAndLoadManager.SetBool ("Is online", value);
			}
		}

		public virtual void DisplayNotification (string text)
		{
			StopCoroutine(notificationTextObject.DisplayRoutine ());
			notificationTextObject.text.text = text;
			StartCoroutine(notificationTextObject.DisplayRoutine ());
		}

		// public virtual void Connect (Callback<Client> onSuccess, Callback<PlayerIOError> onFail)
		// {
		// 	PlayerIO.UseSecureApiRequests = true;
		// 	PlayerIO.Authenticate("burst-my-heart-dyhe2gzfzukop2uttnxokw",
		// 		"public",
		// 		new Dictionary<string, string> {
		// 			{ "userId", "" },
		// 		},
		// 		null,
		// 		onSuccess,
		// 		onFail
		// 	);
		// }
	}
}