﻿/*
	This file defines the manager for building for multiple platforms by clicking on a single button
	It also stores information about the build (like the current development stage, which is used in 'Concepts/MonoBehaviours/Achievements/DevelopmentStageAchievement.cs')
	To start the builds for the platforms, click on the 'Make Builds' button in the Build dropdown at the top of the Unity window or press the Control, Alt, and B keys at the same time
	Change the values of BuildActions within buildActions to control the output location of each build, what platform to target, whether to compress in a .zip file, etc.
*/

using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
#if UNITY_EDITOR
using System.IO;
using Extensions;
using UnityEditor;
using UnityEngine.UI;
using UnityEditor.SceneManagement;
using UnityEditor.Build.Reporting;
#endif

namespace YouAreTheBullet
{
	//[ExecuteAlways]
	public class BuildManager : SingletonMonoBehaviour<BuildManager>
	{
#if UNITY_EDITOR
		public BuildAction[] buildActions;
		public Text versionNumberText;
		static BuildPlayerOptions buildOptions;
#endif
		public DevelopmentStage developmentStage;
		public int versionIndex;
		public string versionNumberPrefix;
		public bool clearDataOnFirstStartup;
		public static bool IsFirstStartup
		{
			get
			{
				return SaveAndLoadManager.GetBool("1st startup", true);
			}
			set
			{
				SaveAndLoadManager.SetBool ("1st startup", value);
			}
		}
		
#if UNITY_EDITOR
		public static string[] GetScenePathsInBuild ()
		{
			List<string> scenePathsInBuild = new List<string>();
			for (int i = 0; i < EditorBuildSettings.scenes.Length; i ++)
			{
				EditorBuildSettingsScene scene = EditorBuildSettings.scenes[i];
				if (scene.enabled)
					scenePathsInBuild.Add(scene.path);
			}
			return scenePathsInBuild.ToArray();
		}

		public static string[] GetAllScenePaths ()
		{
			List<string> scenePaths = new List<string>();
			for (int i = 0; i < EditorBuildSettings.scenes.Length; i ++)
				scenePaths.Add(EditorBuildSettings.scenes[i].path);
			return scenePaths.ToArray();
		}
		
		[MenuItem("Build/Make Builds %&b")]
		public static void Build ()
		{
			Instance._Build ();
		}

		public void _Build ()
		{
			EditorSceneManager.CloseScene(EditorSceneManager.GetActiveScene(), true);
			EditorSceneManager.OpenScene(GetScenePathsInBuild()[0]);
			Instance.versionIndex ++;
			for (int i = 0; i < buildActions.Length; i ++)
			{
				BuildAction buildAction = buildActions[i];
				if (buildAction.enabled)
				{
					EditorPrefs.SetInt("Current build action index", i);
					buildAction.Do ();
				}
			}
			EditorPrefs.SetInt("Current build action index", -1);
		}

		[UnityEditor.Callbacks.DidReloadScripts]
		public static void OnScriptsReload ()
		{
			int currentBuildActionIndex = EditorPrefs.GetInt("Current build action index", -1);
			if (currentBuildActionIndex != -1)
			{
				for (int i = currentBuildActionIndex; i < Instance.buildActions.Length; i ++)
				{
					BuildAction buildAction = instance.buildActions[i];
					if (buildAction.enabled)
					{
						EditorPrefs.SetInt("Current build action index", i);
						buildAction.Do ();
					}
				}
			}
			EditorPrefs.SetInt("Current build action index", -1);
		}
		
		[Serializable]
		public class BuildAction
		{
			public string name;
			public bool enabled;
			public BuildTarget target;
			public string locationPath;
			public BuildOptions[] options;
			public bool removeExtraFolders;
			public bool makeZip;
			public string directoryToZip;
			public string zipLocationPath;
			public bool clearDataOnFirstStartup;
			
			public void Do ()
			{
				if (target == BuildTarget.StandaloneOSX && PlayerSettings.GetScriptingBackend(BuildTargetGroup.Standalone) != ScriptingImplementation.Mono2x)
				{
					PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, ScriptingImplementation.Mono2x);
					return;
				}
				else if (target != BuildTarget.StandaloneOSX && PlayerSettings.GetScriptingBackend(BuildTargetGroup.Standalone) != ScriptingImplementation.IL2CPP)
				{
					PlayerSettings.SetScriptingBackend(BuildTargetGroup.Standalone, ScriptingImplementation.IL2CPP);
					return;
				}
				Instance.clearDataOnFirstStartup = clearDataOnFirstStartup;
				if (instance.versionNumberText != null)
					instance.versionNumberText.text = BuildManager.Instance.versionNumberPrefix + DateTime.Now.Date.ToString("MMdd");
				if (ConfigurationManager.Instance != null)
					ConfigurationManager.instance.canvas.gameObject.SetActive(false);
				EditorSceneManager.MarkAllScenesDirty();
				EditorSceneManager.SaveOpenScenes();
				buildOptions = new BuildPlayerOptions();
				buildOptions.scenes = GetScenePathsInBuild();
				buildOptions.target = target;
				buildOptions.locationPathName = locationPath;
				foreach (BuildOptions option in options)
					buildOptions.options |= option;
				BuildReport report = BuildPipeline.BuildPlayer(buildOptions);
				if (report.summary.result == BuildResult.Cancelled)
				{
					EditorPrefs.SetInt("Current build action index", -1);
					return;
				}
				if (ConfigurationManager.Instance != null)
					ConfigurationManager.instance.canvas.gameObject.SetActive(true);
				AssetDatabase.Refresh();
				if (removeExtraFolders)
				{
					string folderPathPrefix = locationPath.Remove(locationPath.LastIndexOf("/") + 1) + Application.productName;
					Directory.Delete(folderPathPrefix + "_BackUpThisFolder_ButDontShipItWithYourGame");
					Directory.Delete(folderPathPrefix + "_BurstDebugInformation_DoNotShip");
				}
				if (makeZip)
				{
					File.Delete(zipLocationPath);
					SystemExtensions.CompressDirectory (directoryToZip, zipLocationPath);
				}
			}
		}
#endif

		public enum DevelopmentStage
		{
			Alpha,
			Beta,
			Release
		}
	}
}