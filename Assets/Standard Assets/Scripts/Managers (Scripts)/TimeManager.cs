﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	public class TimeManager : SingletonMonoBehaviour<TimeManager>
	{
		public static float TotalGameplayDuration
		{
			get
			{
				return SaveAndLoadManager.GetFloat("Total gameplay duration", 0);
			}
			set
			{
				SaveAndLoadManager.SetFloat("Total gameplay duration", value);
			}
		}
		
		public override void Awake ()
		{
			base.Awake ();
			SetTimeScale (1);
		}
		
		public static void SetTimeScale (float timeScale)
		{
			Time.timeScale = timeScale;
			for (int i = 0; i < _Rigidbody2D.allInstances.Count; i ++)
			{
				Rigidbody2D rigid = _Rigidbody2D.allInstances[i];
				rigid.simulated = timeScale > 0;
			}
		}
		
		public virtual void OnApplicationQuit ()
		{
			TotalGameplayDuration += Time.realtimeSinceStartup;
		}
	}
}