﻿using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Reflection;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Object = UnityEngine.Object;
#if UNITY_EDITOR
using UnityEditor;
#endif

namespace YouAreTheBullet
{
	public class GameManager : SingletonMonoBehaviour<GameManager>, ISavableAndLoadable
	{
		public static bool paused;
		public GameObject[] registeredGos = new GameObject[0];
		[SaveAndLoadValue]
		static string enabledGosString = "";
		[SaveAndLoadValue]
		static string disabledGosString = "";
		public const string STRING_SEPERATOR = "|";
		public float timeScale;
		public static int currentPauses;
		public static IUpdatable[] updatables = new IUpdatable[0];
		public static int framesSinceLoadedScene;
		public const int LAG_FRAMES_AFTER_LOAD_SCENE = 2;
		public static float UnscaledDeltaTime
		{
			get
			{
				if (paused || framesSinceLoadedScene <= LAG_FRAMES_AFTER_LOAD_SCENE)
					return 0;
				else
					return Time.unscaledDeltaTime;
			}
		}
		public static bool initialized;
		public CursorEntry[] cursorEntries = new CursorEntry[0];
		public static Dictionary<string, CursorEntry> cursorEntriesDict = new Dictionary<string, CursorEntry>();
		public static CursorEntry activeCursorEntry;
		public static float cursorMoveSpeed = 0.05f;
		PauseUpdater pauseUpdater = new PauseUpdater();

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			if (!initialized)
			{
				ClearPlayerStats ();
				SaveAndLoadManager.DeleteKey ("Has paused");
				initialized = true;
			}
			if (cursorEntries.Length > 0)
			{
				activeCursorEntry = null;
				cursorEntriesDict.Clear();
				foreach (CursorEntry cursorEntry in cursorEntries)
				{
					cursorEntriesDict.Add(cursorEntry.name, cursorEntry);
					cursorEntry.rectTrs.gameObject.SetActive(false);
				}
				Cursor.visible = false;
				cursorEntriesDict["Default"].SetAsActive ();
			}
			base.Awake ();
		}

		void Update ()
		{
			// try
			// {
				// if (!paused && framesSinceLoadedScene > LAG_FRAMES_AFTER_LOAD_SCENE)
					Physics2D.Simulate(Time.deltaTime);
					Physics2D.SyncTransforms();
				for (int i = 0; i < updatables.Length; i ++)
				{
					IUpdatable updatable = updatables[i];
					updatable.DoUpdate ();
				}
				if (ObjectPool.instance != null && ObjectPool.instance.enabled)
					ObjectPool.instance.DoUpdate ();
				GameCamera.Instance.DoUpdate ();
				InputSystem.Update ();
				framesSinceLoadedScene ++;
			// }
			// catch (Exception e)
			// {
			// 	print(e.Message + "\n" + e.StackTrace);
			// }
		}

		public void LoadScene (string name)
		{
			if (instance != this)
			{
				instance.LoadScene (name);
				return;
			}
			if (name == "Title")
				ClearPlayerStats ();
			framesSinceLoadedScene = 0;
			SceneManager.LoadScene(name);
		}

		public void ClearPlayerStats ()
		{
			SaveAndLoadManager.DeleteKey ("1 score");
			SaveAndLoadManager.DeleteKey ("2 score");
			SaveAndLoadManager.DeleteKey ("1 wins in a row");
			SaveAndLoadManager.DeleteKey ("2 wins in a row");
			SaveAndLoadManager.DeleteKey ("1 move speed multiplier");
			SaveAndLoadManager.DeleteKey ("2 move speed multiplier");
		}

		public void LoadScene (int index)
		{
			LoadScene (SceneManager.GetSceneByBuildIndex(index).name);
		}

		public void ReloadActiveScene ()
		{
			LoadScene (SceneManager.GetActiveScene().name);
		}

		public void PauseGame (int pauses)
		{
			// currentPauses = Mathf.Clamp(currentPauses + pauses, 0, int.MaxValue);
			currentPauses = pauses;
			bool previousPaused = paused;
			paused = currentPauses > 0;
			Time.timeScale = timeScale * (1 - paused.GetHashCode());
			AudioListener.pause = paused;
			if (paused)
			{
				if (!previousPaused)
					GameManager.updatables = GameManager.updatables.Add(pauseUpdater);
				AudioListener.volume = 0;
			}
			else
			{
				if (previousPaused)
					GameManager.updatables = GameManager.updatables.Remove(pauseUpdater);
				AudioManager.instance.UpdateAudioListener ();
			}
		}

		public void Quit ()
		{
			Application.Quit();
		}

		public void OnApplicationQuit ()
		{
			ClearPlayerStats ();
			SaveAndLoadManager.DeleteKey ("Has paused");
			SaveAndLoadManager.DeleteKey ("Is online");
			// SaveAndLoadManager.instance.Save ();
		}

		public void SetGosActive ()
		{
			if (instance != this)
			{
				instance.SetGosActive ();
				return;
			}
			string[] stringSeperators = { STRING_SEPERATOR };
			string[] enabledGos = enabledGosString.Split(stringSeperators, StringSplitOptions.None);
			foreach (string goName in enabledGos)
			{
				for (int i = 0; i < registeredGos.Length; i ++)
				{
					GameObject registeredGo = registeredGos[i];
					if (goName == registeredGo.name)
					{
						registeredGo.SetActive(true);
						break;
					}
				}
			}
			string[] disabledGos = disabledGosString.Split(stringSeperators, StringSplitOptions.None);
			foreach (string goName in disabledGos)
			{
				GameObject go = GameObject.Find(goName);
				if (go != null)
					go.SetActive(false);
			}
		}
		
		public void ActivateGoForever (GameObject go)
		{
			go.SetActive(true);
			ActivateGoForever (go.name);
		}
		
		public void DeactivateGoForever (GameObject go)
		{
			go.SetActive(false);
			DeactivateGoForever (go.name);
		}
		
		public void ActivateGoForever (string goName)
		{
			disabledGosString = disabledGosString.Replace(STRING_SEPERATOR + goName, "");
			if (!enabledGosString.Contains(goName))
				enabledGosString += STRING_SEPERATOR + goName;
		}
		
		public void DeactivateGoForever (string goName)
		{
			enabledGosString = enabledGosString.Replace(STRING_SEPERATOR + goName, "");
			if (!disabledGosString.Contains(goName))
				disabledGosString += STRING_SEPERATOR + goName;
		}

		public static float ClampAngle (float ang, float min, float max)
		{
			ang = WrapAngle(ang);
			min = WrapAngle(min);
			max = WrapAngle(max);
			float minDist = Mathf.Min(Mathf.DeltaAngle(ang, min), Mathf.DeltaAngle(ang, max));
			if (WrapAngle(ang + Mathf.DeltaAngle(ang, minDist)) == min)
				return min;
			else if (WrapAngle(ang + Mathf.DeltaAngle(ang, minDist)) == max)
				return max;
			else
				return ang;
		}

		public static float WrapAngle (float ang)
		{
			if (ang < 0)
				ang += 360;
			else if (ang > 360)
				ang = 360 - ang;
			return ang;
		}

		public static void SetGameObjectActive (string name)
		{
			GameObject.Find(name).SetActive(true);
		}

		public static void SetGameObjectInactive (string name)
		{
			GameObject.Find(name).SetActive(false);
		}

		void OnDestroy ()
		{
			StopAllCoroutines();
			if (instance == this)
				OnApplicationQuit ();
		}

		public void ClickButton (Button button)
		{
			button.onClick.Invoke();
		}
		
#if UNITY_EDITOR
		public static void DestroyOnNextEditorUpdate (Object obj)
		{
			EditorApplication.update += () => { if (obj == null) return; DestroyObject (obj); };
		}

		static void DestroyObject (Object obj)
		{
			if (obj == null)
				return;
			EditorApplication.update -= () => { DestroyObject (obj); };
			DestroyImmediate(obj);
		}
#endif

		class PauseUpdater : IUpdatable
		{
			bool previousWorldMapInput;

			public void DoUpdate ()
			{
				bool worldMapInput = InputManager.GetWorldMapInput();
				if (worldMapInput && !previousWorldMapInput)
				{
					if (!WorldMap.isOpen)
						WorldMap.instance.Open ();
					else
						WorldMap.instance.Close ();
				}
				previousWorldMapInput = worldMapInput;
			}
		}

		[Serializable]
		public class CursorEntry
		{
			public string name;
			public RectTransform rectTrs;

			public void SetAsActive ()
			{
				if (activeCursorEntry != null)
					activeCursorEntry.rectTrs.gameObject.SetActive(false);
				rectTrs.gameObject.SetActive(true);
				activeCursorEntry = this;
			}
		}
	}
}
