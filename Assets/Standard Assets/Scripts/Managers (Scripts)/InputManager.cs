﻿using System;
using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using UnityEngine.InputSystem.EnhancedTouch;

namespace YouAreTheBullet
{
	public class InputManager : SingletonUpdateWhileEnabled<InputManager>
	{
		public InputSettings settings;
		public static InputDevice[] inputDevices = new InputDevice[0];
		public static bool UsingMouse
		{
			get
			{
#if UNITY_EDITOR
				if (Instance.simulateTouch)
					return false;
#endif
				return Mouse.current != null;
			}
		}
		public static bool UsingKeyboard
		{
			get
			{
#if UNITY_EDITOR
				if (Instance.simulateTouch)
					return false;
#endif
				return Keyboard.current != null;
			}
		}
		public static bool UsingGamepad
		{
			get
			{
#if UNITY_EDITOR
				if (Instance.simulateTouch)
					return false;
#endif
				return Gamepad.current != null;
			}
		}
		public static bool UsingTouchscreen
		{
			get
			{
#if UNITY_EDITOR
				if (Instance.simulateTouch)
					return true;
#endif
				return Touchscreen.current != null && Application.isMobilePlatform;
			}
		}
		public float joystickDeadzone;
		public Timer cursorUnhideTimer;
		public GameObject phoneControlsUIGo;
		public VirtualJoystick moveVirtualJoystick;
		public VirtualJoystick aimVirtualJoystick;
#if UNITY_EDITOR
		public bool simulateTouch;
		public static TouchSimulation touchSimulation;
#endif
		Vector2 previousMousePosition;
		bool previousUsingGamepad;
		bool previousUsingTouchscreen;

		public override void Awake ()
		{
			if (instance != null)
				return;
			base.Awake ();
			if (UsingKeyboard)
				inputDevices = inputDevices.Add(InputDevice.Keyboard);
			cursorUnhideTimer.onFinished += (object[] args) => { Cursor.visible = false; };
			SceneManager.sceneLoaded += OnSceneLoaded;
		}

		void OnSceneLoaded (Scene scene = new Scene(), LoadSceneMode loadMode = LoadSceneMode.Single)
		{
#if UNITY_EDITOR
			if (simulateTouch)
			{
				EnhancedTouchSupport.Enable();
				touchSimulation = gameObject.AddComponent<TouchSimulation>();
				TouchSimulation.Enable();
			}
#endif
			phoneControlsUIGo.SetActive(UsingTouchscreen);
		}

		public static float GetZoomInput ()
		{
			if (UsingGamepad)
				return GetZoomInput(Gamepad.current);
			else
				return GetZoomInput(Mouse.current);
		}

		public static float GetZoomInput (UnityEngine.InputSystem.InputDevice inputDevice)
		{
			Gamepad gamepad = inputDevice as Gamepad;
			if (gamepad != null)
			{
				float output = 0;
				if (gamepad.leftTrigger.isPressed)
					output = 1;
				if (gamepad.rightTrigger.isPressed)
					output ++;
				return output;
			}
			else
			{
				Mouse mouse = inputDevice as Mouse;
				return mouse.scroll.y.ReadValue();
			}
		}

		public static float GetZoomInput (string deviceName)
		{
			return GetZoomInput(GetDevice(deviceName));
		}

		public static bool GetWorldMapInput ()
		{
			if (UsingGamepad)
				return GetWorldMapInput(Gamepad.current);
			else
				return GetWorldMapInput(Keyboard.current);
		}

		public static bool GetWorldMapInput (UnityEngine.InputSystem.InputDevice inputDevice)
		{
			Gamepad gamepad = inputDevice as Gamepad;
			if (gamepad != null)
				return gamepad.leftShoulder.isPressed;
			else
			{
				Keyboard keyboard = inputDevice as Keyboard;
				return keyboard.tabKey.isPressed;
			}
		}

		public static bool GetWorldMapInput (string deviceName)
		{
			return GetWorldMapInput(GetDevice(deviceName));
		}

		public static bool GetSwitchPositionsInput ()
		{
			if (UsingGamepad)
				return GetSwitchPositionsInput(Gamepad.current);
			else
				return GetSwitchPositionsInput(Keyboard.current);
		}

		public static bool GetSwitchPositionsInput (UnityEngine.InputSystem.InputDevice inputDevice)
		{
			Gamepad gamepad = inputDevice as Gamepad;
			if (gamepad != null)
				return gamepad.leftShoulder.isPressed || gamepad.rightShoulder.isPressed;
			else
			{
				Keyboard keyboard = inputDevice as Keyboard;
				return keyboard.spaceKey.isPressed;
			}
		}

		public static bool GetSwitchPositionsInput (string deviceName)
		{
			return GetSwitchPositionsInput(GetDevice(deviceName));
		}

		public static bool GetSwitchControlInput (UnityEngine.InputSystem.InputDevice inputDevice)
		{
			Gamepad gamepad = inputDevice as Gamepad;
			if (gamepad != null)
				return gamepad.leftShoulder.isPressed || gamepad.rightShoulder.isPressed;
			else
			{
				Keyboard keyboard = inputDevice as Keyboard;
				return keyboard.spaceKey.isPressed;
			}
		}

		public static bool GetSwitchControlInput (string deviceName)
		{
			return GetSwitchControlInput(GetDevice(deviceName));
		}

		public static bool GetBodySwitchDimensionsInput (UnityEngine.InputSystem.InputDevice inputDevice)
		{
			Gamepad gamepad = inputDevice as Gamepad;
			if (gamepad != null)
				return gamepad.leftShoulder.isPressed;
			else
			{
				Keyboard keyboard = inputDevice as Keyboard;
				return keyboard.leftAltKey.isPressed;
			}
		}

		public static bool GetBodySwitchDimensionsInput (string deviceName)
		{
			return GetBodySwitchDimensionsInput(GetDevice(deviceName));
		}

		public static bool GetBulletSwitchDimensionsInput (UnityEngine.InputSystem.InputDevice inputDevice)
		{
			Gamepad gamepad = inputDevice as Gamepad;
			if (gamepad != null)
				return gamepad.rightShoulder.isPressed;
			else
			{
				Keyboard keyboard = inputDevice as Keyboard;
				return keyboard.rightAltKey.isPressed;
			}
		}

		public static bool GetBulletSwitchDimensionsInput (string deviceName)
		{
			return GetBulletSwitchDimensionsInput(GetDevice(deviceName));
		}

		public static bool GetSubmitInput ()
		{
			if (UsingGamepad)
				return GetSubmitInput(Gamepad.current);
			else
				return GetSubmitInput(Keyboard.current);
		}

		public static bool GetSubmitInput (UnityEngine.InputSystem.InputDevice inputDevice)
		{
			Gamepad gamepad = inputDevice as Gamepad;
			if (gamepad != null)
				return gamepad.buttonEast.isPressed;
			else
			{
				Keyboard keyboard = inputDevice as Keyboard;
				return keyboard.enterKey.isPressed;
			}
		}

		public static bool GetSubmitInput (string deviceName)
		{
			return GetSubmitInput(GetDevice(deviceName));
		}

		public static bool GetInteractInput ()
		{
			if (UsingGamepad)
				return GetInteractInput(Gamepad.current);
			else
				return GetInteractInput(Keyboard.current);
		}

		public static bool GetInteractInput (UnityEngine.InputSystem.InputDevice inputDevice)
		{
			Gamepad gamepad = inputDevice as Gamepad;
			if (gamepad != null)
				return gamepad.buttonEast.isPressed;
			else
			{
				Keyboard keyboard = inputDevice as Keyboard;
				return keyboard.spaceKey.isPressed;
			}
		}

		public static bool GetInteractInput (string deviceName)
		{
			return GetInteractInput(GetDevice(deviceName));
		}

		public static Vector2 GetMoveInput ()
		{
			if (UsingGamepad)
				return GetMoveInput(Gamepad.current);
			else
				return GetMoveInput(Keyboard.current);
		}

		public static Vector2 GetMoveInput (UnityEngine.InputSystem.InputDevice inputDevice)
		{
			Gamepad gamepad = inputDevice as Gamepad;
			if (gamepad != null)
				return gamepad.leftStick.ReadValue();
			else
			{
				Keyboard keyboard = inputDevice as Keyboard;
				Vector2 output = new Vector2();
				if (keyboard.aKey.isPressed)
					output.x --;
				if (keyboard.dKey.isPressed)
					output.x ++;
				if (keyboard.sKey.isPressed)
					output.y --;
				if (keyboard.wKey.isPressed)
					output.y ++;
				return output;
			}
		}

		public static Vector2 GetMoveInput (string deviceName)
		{
			return GetMoveInput(GetDevice(deviceName));
		}

		public static Vector2 GetAimInput ()
		{
			if (UsingGamepad)
				return GetAimInput(Gamepad.current);
			else
				return GetAimInput(Keyboard.current);
		}

		public static Vector2 GetAimInput (UnityEngine.InputSystem.InputDevice inputDevice)
		{
			Gamepad gamepad = inputDevice as Gamepad;
			if (gamepad != null)
				return gamepad.rightStick.ReadValue();
			else
			{
				Keyboard keyboard = inputDevice as Keyboard;
				Vector2 output = new Vector2();
				if (keyboard.leftArrowKey.isPressed)
					output.x --;
				if (keyboard.rightArrowKey.isPressed)
					output.x ++;
				if (keyboard.downArrowKey.isPressed)
					output.y --;
				if (keyboard.upArrowKey.isPressed)
					output.y ++;
				return output;
			}
		}

		public static Vector2 GetAimInput (string deviceName)
		{
			return GetAimInput(GetDevice(deviceName));
		}

		public static UnityEngine.InputSystem.InputDevice GetDevice (string name)
		{
			return InputSystem.GetDevice(name);
		}

		public override void DoUpdate ()
		{
			Vector2 mousePosition = new Vector2();
			if (UsingMouse)
				mousePosition = Mouse.current.position.ReadValue();
			bool usingGamepad = UsingGamepad;
			bool usingTouchscreen = UsingTouchscreen;
			if (usingGamepad)
			{
				if (!previousUsingGamepad)
				{
					for (int i = 0; i < Rigidbody2DController.controllableInstances.Count; i ++)
					{
						Rigidbody2DController Rigidbody2DController = Rigidbody2DController.controllableInstances[i];
						Rigidbody2DController.SetInputDevice ();
					}
					Cursor.visible = false;
					inputDevices = inputDevices.Add(InputDevice.Gamepad);
					inputDevices = inputDevices._Sort(new InputDeviceSorter());
					UpdateTexts ();
				}
			}
			else if (previousUsingGamepad)
			{
				for (int i = 0; i < Rigidbody2DController.controllableInstances.Count; i ++)
				{
					Rigidbody2DController Rigidbody2DController = Rigidbody2DController.controllableInstances[i];
					Rigidbody2DController.SetInputDevice ();
				}
				inputDevices = inputDevices.Remove(InputDevice.Gamepad);
				UpdateTexts ();
			}
			if (usingTouchscreen)
			{
#if UNITY_EDITOR
				if (simulateTouch && !touchSimulation.simulatedTouchscreen.added)
					InputSystem.AddDevice(touchSimulation.simulatedTouchscreen);
#endif
				if (!previousUsingTouchscreen)
				{
					Cursor.visible = false;
					inputDevices = inputDevices.Add(InputDevice.Touchscreen);
					inputDevices = inputDevices._Sort(new InputDeviceSorter());
					UpdateTexts ();
					phoneControlsUIGo.SetActive(true);
				}
			}
			else if (previousUsingTouchscreen)
			{
				inputDevices = inputDevices.Remove(InputDevice.Touchscreen);
				UpdateTexts ();
				phoneControlsUIGo.SetActive(false);
			}
			if (usingGamepad || usingTouchscreen)
			{
				if (mousePosition != previousMousePosition)
				{
					Cursor.visible = true;
					cursorUnhideTimer.Reset ();
					cursorUnhideTimer.Start ();
				}
			}
			else
				Cursor.visible = true;
			previousUsingGamepad = usingGamepad;
			previousUsingTouchscreen = usingTouchscreen;
			previousMousePosition = mousePosition;
		}

		void UpdateTexts ()
		{
			for (int i = 0; i < _Text.instances.Length; i ++)
			{
				_Text text = _Text.instances[i];
				text.UpdateText ();
			}
		}

		public override void OnDisable ()
		{
			base.OnDisable ();
			cursorUnhideTimer.onFinished -= (object[] args) => { Cursor.visible = false; };
#if UNITY_EDITOR
			if (instance == null)
				inputDevices = new InputDevice[0];
#endif
		}

		class InputDeviceSorter : IComparer<InputDevice>
		{
			public int Compare (InputDevice inputDevice1, InputDevice inputDevice2)
			{
				return (int) Mathf.Sign(inputDevice1.GetHashCode() - inputDevice2.GetHashCode());
			}
		}
	}

	public enum InputDevice
	{
		Keyboard,
		Gamepad,
		Touchscreen
	}
}