﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;

namespace YouAreTheBullet
{
	public class AudioManager : SingletonMonoBehaviour<AudioManager>
	{
		public float Volume
		{
			get
			{
				return SaveAndLoadManager.GetFloat("Volume", 1);
			}
			set
			{
				SaveAndLoadManager.SetFloat("Volume", value);
			}
		}
		public bool Mute
		{
			get
			{
				return SaveAndLoadManager.GetInt("Mute", 0) == 1;
			}
			set
			{
				SaveAndLoadManager.SetInt("Mute", value.GetHashCode());
			}
		}
		public SoundEffect soundEffectPrefab;
		public static SoundEffect[] soundEffects = new SoundEffect[0];
		public AnimationCurve soundEffectVolumeOverCount;
		public TextMesh muteToggleTextMesh;
		public Transform muteToggleTrs;

		public override void Awake ()
		{
			UpdateAudioListener ();
			UpdateMuteText ();
			if (Instance != null && instance != this)
			{
				instance.muteToggleTextMesh = muteToggleTextMesh;
				instance.muteToggleTrs = muteToggleTrs;
			}
			else
			{
				base.Awake ();
				soundEffects = new SoundEffect[0];
			}
		}

		public virtual void UpdateAudioListener ()
		{
			AudioListener.pause = Mute;
			if (Mute)
				AudioListener.volume = 0;
			else
				AudioListener.volume = Volume;
		}

		public virtual void ToggleMute ()
		{
			if (instance != this)
			{
				instance.ToggleMute ();
				return;
			}
			Mute = !Mute;
			UpdateAudioListener ();
			UpdateMuteText ();
		}

		public virtual void UpdateMuteText ()
		{
			if (muteToggleTextMesh == null)
				return;
			if (Mute)
			{
				muteToggleTextMesh.text = "Unmute";
				muteToggleTrs.localScale = new Vector3(0.00025f, 0.00025f, 1);
			}
			else
			{
				muteToggleTextMesh.text = "Mute";
				muteToggleTrs.localScale = new Vector3(0.0004f, 0.0004f, 1);
			}
		}
		
		public virtual SoundEffect MakeSoundEffect (AudioClip audioClip, SoundEffect.Settings settings = null, Vector2 position = new Vector2())
		{
			if (Mute)
				return null;
			SoundEffect output = ObjectPool.Instance.SpawnComponent<SoundEffect>(soundEffectPrefab.prefabIndex, position);
			if (output == null)
				return null;
			soundEffects = soundEffects.Add(output);
			output.audio.volume = soundEffectVolumeOverCount.Evaluate(soundEffects.Length);
			if (settings != null)
			{
				output.audio.volume *= settings.volume;
				output.audio.pitch *= settings.pitch;
			}
			output.audio.clip = audioClip;
			output.audio.Play();
			ObjectPool.instance.DelayDespawn (output.prefabIndex, output.gameObject, output.trs, audioClip.length);
			return output;
		}
	}
}