#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;
using System.Collections;
using System.Collections.Generic;
using UnityEditor.SceneManagement;

namespace Extensions
{
	public static class PrefabExtensions
	{
		public static GameObject ClonePrefabInstance (GameObject prefabInstanceGo)
		{
			List<AddedGameObject> addedGos = PrefabUtility.GetAddedGameObjects(prefabInstanceGo);
			List<AddedComponent> addedComponents = PrefabUtility.GetAddedComponents(prefabInstanceGo);
			List<RemovedComponent> removedComponents = PrefabUtility.GetRemovedComponents(prefabInstanceGo);
			PropertyModification[] propertyModifications = PrefabUtility.GetPropertyModifications(prefabInstanceGo);
			GameObject output = (GameObject) PrefabUtility.InstantiatePrefab(PrefabUtility.GetCorrespondingObjectFromSource(prefabInstanceGo));
			Transform prefabInstanceTrs = prefabInstanceGo.GetComponent<Transform>();
			foreach (AddedGameObject addedGo in addedGos)
			{
				Transform addedTrs = addedGo.instanceGameObject.GetComponent<Transform>();
				if (PrefabUtility.GetPrefabInstanceStatus(addedGo.instanceGameObject) == PrefabInstanceStatus.NotAPrefab)
					Object.Instantiate(addedGo.instanceGameObject, TransformExtensions.FindEquivalentChild(prefabInstanceTrs, addedTrs, output.GetComponent<Transform>()));
				else
				{
					Transform clonedTrs = ClonePrefabInstance(addedGo.instanceGameObject).GetComponent<Transform>();
					clonedTrs.SetParent(TransformExtensions.FindEquivalentChild(prefabInstanceTrs, addedTrs, output.GetComponent<Transform>()));
					clonedTrs.position = addedTrs.position;
					clonedTrs.rotation = addedTrs.rotation;
					clonedTrs.localScale = addedTrs.localScale;
				}
			}
			foreach (AddedComponent addedComponent in addedComponents)
				output.AddComponent(addedComponent.instanceComponent.GetType());
			foreach (RemovedComponent removedComponent in removedComponents)
				Object.DestroyImmediate(removedComponent.containingInstanceGameObject.GetComponent(removedComponent.assetComponent.GetType()));
			for (int i = 0; i < propertyModifications.Length; i ++)
			{
				PropertyModification propertyModification = propertyModifications[i];
				if (propertyModification.objectReference == null || propertyModification.target == null)
				{
					propertyModifications = propertyModifications.RemoveAt(i);
					i --;
				}
			}
			if (propertyModifications.Length > 0)
				PrefabUtility.SetPropertyModifications(output, propertyModifications);
			output.name = prefabInstanceGo.name;
			output.layer = prefabInstanceGo.layer;
			return output;
		}
	}
}
#endif