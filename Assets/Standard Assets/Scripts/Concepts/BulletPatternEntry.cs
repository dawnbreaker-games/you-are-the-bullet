using System;
using UnityEngine;

namespace YouAreTheBullet
{
	[Serializable]
	public class BulletPatternEntry : IUpdatable
	{
		public string name;
		public Transform spawner;
		public BulletPattern bulletPattern;
		public Bullet bulletPrefab;
		public float reloadDuration;
		float lastShotTime;

		public void DoUpdate ()
		{
			if (Time.time - lastShotTime >= reloadDuration)
				Shoot ();
		}

		public void Shoot ()
		{
			lastShotTime = Time.time;
			bulletPattern.Shoot (spawner, bulletPrefab);
		}
	}
}