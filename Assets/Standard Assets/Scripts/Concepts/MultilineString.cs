using System;
using UnityEngine;

[Serializable]
public struct MultilineString
{
	[Multiline]
	public string value;
}