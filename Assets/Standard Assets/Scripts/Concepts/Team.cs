using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
    public class Team : MonoBehaviour
    {
		public Color color;
		public Player representative;
		public Player[] representatives;
        public Team opponent;
    }
}