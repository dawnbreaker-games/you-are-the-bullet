﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	public class Hazard : Spawnable
	{
		[Header("Hazard")]
		public float damage;
		
		public virtual void OnCollisionEnter2D (Collision2D coll)
		{
			IDestructable destructable = coll.collider.GetComponentInParent<IDestructable>();
			if (destructable != null)
			{
				// Player player = destructable as Player;
				// if (player != null)
				// {
				// 	if (player.rigid.velocity == Vector2.zero || Vector2.Dot(player.rigid.velocity, coll.GetContact(0).point - (Vector2) player.trs.position) < 0)
				// 		destructable.TakeDamage (damage);
				// }
				// else
					destructable.TakeDamage (damage);
			}
		}
	}
}