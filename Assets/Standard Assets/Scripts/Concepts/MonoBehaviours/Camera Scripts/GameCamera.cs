using Extensions;
using UnityEngine;

namespace YouAreTheBullet
{
	public class GameCamera  : CameraScript
	{
		public new static GameCamera instance;
		public new static GameCamera Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<GameCamera>();
				return instance;
			}
		}

		public void DoUpdate ()
		{
			HandlePosition ();
		}

		public override void HandlePosition ()
		{
			trs.position = Player.instance.trs.position.SetZ(trs.position.z);
			base.HandlePosition ();
		}
	}
}