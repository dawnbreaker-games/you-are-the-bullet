using Extensions;
using UnityEngine;

namespace YouAreTheBullet
{
	public class Area : SingletonMonoBehaviour<Area>
	{
		public Transform trs;
		public Renderer colorGradient2DRenderer;
		public ColorGradient2D colorGradient2D;
		public Vector2Int colorKeys;
		public Vector2Int alphaKeys;
		public Vector2Int textureSize;
		public float pixelsPerUnit;
		public bool generateRandomGradient;

#if UNITY_EDITOR
		void OnValidate ()
		{
			if (generateRandomGradient)
				colorGradient2D = ColorGradient2D.GenerateRandom(colorKeys.x, alphaKeys.x, colorKeys.y, alphaKeys.y);
		}
#endif
		
		public virtual void OnEnable ()
		{
			if (generateRandomGradient)
				colorGradient2D = ColorGradient2D.GenerateRandom(colorKeys.x, alphaKeys.x, colorKeys.y, alphaKeys.y);
			colorGradient2DRenderer.material.mainTexture = colorGradient2D.MakeSprite(textureSize, pixelsPerUnit).texture;
		}
	}
}
