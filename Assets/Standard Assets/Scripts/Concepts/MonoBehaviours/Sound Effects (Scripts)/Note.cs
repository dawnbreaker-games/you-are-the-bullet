using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	public class Note : SoundEffect
	{
		public static uint instanceCount;
		public int sampleRate;
		public float frequency;
		public float length;
		public AnimationCurve volumeCurve;
		int position;

		void OnAudioRead (float[] data)
		{
			int count = 0;
			while (count < data.Length)
			{
				data[count] = Mathf.Sin(2 * Mathf.PI * frequency * position / sampleRate) * (volumeCurve.Evaluate((float) position / data.Length) * (1f / instanceCount));
				position ++;
				count ++;
			}
		}

		void OnAudioSetPosition (int newPosition)
		{
			position = newPosition;
		}

		public void Init ()
		{
			instanceCount ++;
			position = 0;
			AudioClip clip = AudioClip.Create("Note", (int) (sampleRate * length), 1, sampleRate, false, OnAudioRead, OnAudioSetPosition);
			audio.clip = clip;
			audio.Play();
			ObjectPool.instance.DelayDespawn (prefabIndex, gameObject, trs, length);
		}

		public override void OnDisable ()
		{
			instanceCount --;
		}
	}
}