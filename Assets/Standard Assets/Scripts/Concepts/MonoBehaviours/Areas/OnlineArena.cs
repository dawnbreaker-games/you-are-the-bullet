using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	[DisallowMultipleComponent]
    public class OnlineArena : Area
    {
		public new static OnlineArena instance;
		public new static OnlineArena Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<OnlineArena>();
				return instance;
			}
		}
    	public uint areaPerPlayer;
    	public float lerpRate;
		[HideInInspector]
    	public float desiredRadius;
    	float desiredArea;
		SizeUpdater sizeUpdater;

		public void SetSize (uint playerCount)
		{
			GameManager.updatables = GameManager.updatables.Remove(sizeUpdater);
			desiredArea = areaPerPlayer * playerCount;
			desiredRadius = Mathf.Sqrt(desiredArea / Mathf.PI);
			sizeUpdater = new SizeUpdater(playerCount);
			GameManager.updatables = GameManager.updatables.Add(sizeUpdater);
		}

		void OnDestroy ()
		{
			GameManager.updatables = GameManager.updatables.Remove(sizeUpdater);
		}

		class SizeUpdater : IUpdatable
		{
			uint playerCount;

			public SizeUpdater (uint playerCount)
			{
				this.playerCount = playerCount;
			}

			public void DoUpdate ()
			{
				instance.trs.localScale = Vector3.one * Mathf.Lerp(instance.trs.localScale.x, instance.desiredRadius * 2, instance.lerpRate * Time.unscaledDeltaTime);
				if (instance.trs.localScale.x == instance.desiredRadius * 2)
					GameManager.updatables = GameManager.updatables.Remove(this);
			}
		}
    }
}