using Extensions;
using UnityEngine;
using UnityEngine.InputSystem;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	[RequireComponent(typeof(Rigidbody2D))]
	[DisallowMultipleComponent]
	public class Rigidbody2DController : UpdateWhileEnabled
	{
		public Transform trs;
		public Rigidbody2D rigid;
		public Collider2D collider;
		public SpriteRenderer spriteRenderer;
		public float moveSpeed;
		public DistanceJoint2D distanceJoint;
		public Player player;
		public bool canControl;
		public int inputterId;
		[HideInInspector]
		public string inputDeviceName;
		public SpriteRenderer controllingIndicator;
		public float radius;
		[HideInInspector]
		public Vector2 extraVelocity;
		public bool inFirstDimension = true;
		[HideInInspector]
		public Vector2 moveInput;
		public static List<Rigidbody2DController> controllableInstances = new List<Rigidbody2DController>();
		Vector2 previousMoveInput;

		void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
			{
				if (trs == null)
					trs = GetComponent<Transform>();
				if (rigid == null)
					rigid = GetComponent<Rigidbody2D>();
				if (distanceJoint == null)
					distanceJoint = GetComponent<DistanceJoint2D>();
				if (spriteRenderer == null)
					spriteRenderer = GetComponent<SpriteRenderer>();
				return;
			}
#endif
			SetInputDevice ();
		}

		public override void DoUpdate ()
		{
			if (inputterId == -1 || string.IsNullOrEmpty(inputDeviceName))
				return;
			HandleMovement ();
			trs.position = Vector2.ClampMagnitude(trs.position, Area.Instance.trs.lossyScale.x / 2 - radius);
		}

		public virtual void OnDestroy ()
		{
			if (canControl)
				controllableInstances.Remove(this);
		}

		public void SetInputDevice ()
		{
			if (inputterId == 0)
			{
				if (Gamepad.all.Count > 1)
					inputDeviceName = Gamepad.all[0].name;
				else
					inputDeviceName = Keyboard.current.name;
			}
			else if (inputterId == 1)
			{
				if (Gamepad.all.Count > 1)
					inputDeviceName = Gamepad.all[1].name;
				else if (Gamepad.all.Count == 1)
					inputDeviceName = Gamepad.all[0].name;
				else
					inputDeviceName = Keyboard.current.name;
			}
		}

		public virtual void HandleMovement ()
		{
			if (!canControl)
				moveInput = previousMoveInput;
			Move (moveInput);
			previousMoveInput = moveInput;
		}

		public virtual void Move (Vector2 move)
		{
			move = Vector2.ClampMagnitude(move, 1);
			rigid.velocity = move * moveSpeed + extraVelocity;
		}
	}
}