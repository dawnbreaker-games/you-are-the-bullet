using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.InputSystem;
using UnityEngine.SceneManagement;

namespace YouAreTheBullet
{
	[DisallowMultipleComponent]
	public class Entity : UpdateWhileEnabled, IDestructable
	{
		[Header("Entity")]
		public Transform trs;
		public float moveSpeed;
		public Rigidbody2D rigid;
		public Animator animator;
		[HideInInspector]
		public bool isDead;
		[Header("IDestructable")]
		public uint maxHp;
		public uint MaxHp
		{
			get
			{
				return maxHp;
			}
			set
			{
				maxHp = value;
			}
		}
		float hp;
		public float Hp
		{
			get
			{
				return hp;
			}
			set
			{
				hp = value;
			}
		}
		public Action<Entity> onDeath;

        public override void OnEnable ()
        {
            base.OnEnable ();
            hp = maxHp;
        }

		public override void DoUpdate ()
		{
			HandleMovement ();
		}

		public virtual void HandleMovement ()
		{
		}

		public virtual void Move (Vector2 move)
		{
			move = Vector2.ClampMagnitude(move, 1);
			rigid.velocity = move * moveSpeed;
		}
	
		public void TakeDamage (float amount)
		{
			if (!isDead)
			{
				hp -= amount;
				if (hp <= 0)
					Death ();
			}
		}
		
		public virtual void Death ()
		{
			isDead = true;
			if (onDeath != null)
				onDeath (this);
		}
	}
}