using TMPro;
using UnityEngine;

namespace YouAreTheBullet
{
	public class SinglePlayerGameMode : SingletonMonoBehaviour<SinglePlayerGameMode>
	{
		[Header("SinglePlayerGameMode")]
		public TMP_Text scoreText;
		public TMP_Text highscoreText;
		uint score;
		public uint Highscore
		{
			get
			{
				return (uint) SaveAndLoadManager.GetInt(name + " highscore", 0);
			}
			set
			{
				SaveAndLoadManager.SetInt (name + " highscore", (int) value);
			}
		}

		public override void Awake ()
		{
			base.Awake ();
			highscoreText.text = "Highscore: " + Highscore;
		}

		public void SetScore (uint amount)
		{
			score = amount;
			scoreText.text = "Score: " + amount;
			if (score > Highscore)
			{
				Highscore = score;
				highscoreText.text = "Highscore: " + score;
			}
		}

		public void AddScore (int amount = 1)
		{
			SetScore ((uint) (score + amount));
		}
	}
}
