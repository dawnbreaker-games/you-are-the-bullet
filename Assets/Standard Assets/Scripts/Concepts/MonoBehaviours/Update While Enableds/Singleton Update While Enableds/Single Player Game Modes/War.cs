using System;
using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;
using UnityEngine.SceneManagement;
using Random = UnityEngine.Random;

namespace YouAreTheBullet
{
	public class War : SinglePlayerGameMode
	{
		public new static War instance;
		public new static War Instance
		{
			get
			{
				if (instance == null)
					instance = FindObjectOfType<War>();
				return instance;
			}
		}
		public uint deathPenalty = 1;
		public float addToIdealDifficulty = 1;
		public float idealDifficulty = 1;
		public EnemyEntry[] enemyEntries = new EnemyEntry[0];
		Enemy[] enemies = new Enemy[0];
		uint enemyCount;

		public override void Awake ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			base.Awake ();
			if (Highscore > deathPenalty)
			{
				idealDifficulty += addToIdealDifficulty * (Highscore - deathPenalty);
				SetScore (Highscore - deathPenalty);
			}
			SpawnEnemies ();
		}

		void SpawnEnemies ()
		{
			float difficulty = 0;
			List<EnemyEntry> _enemyEntries = new List<EnemyEntry>(enemyEntries);
			while (difficulty < idealDifficulty)
			{
				int indexOfNextEnemyEntry = Random.Range(0, _enemyEntries.Count);
				EnemyEntry nextEnemyEntry = _enemyEntries[indexOfNextEnemyEntry];
				if (difficulty + nextEnemyEntry.enemyPrefab.difficulty > idealDifficulty)
				{
					_enemyEntries.RemoveAt(indexOfNextEnemyEntry);
					if (_enemyEntries.Count == 0)
						break;
				}
				else
				{
					difficulty += nextEnemyEntry.enemyPrefab.difficulty;
					GameManager.updatables = GameManager.updatables.Add(nextEnemyEntry);
					enemyCount ++;
				}
			}
			idealDifficulty += addToIdealDifficulty;
		}

		void OnEnemyKilled (Entity entity)
		{
			instance.enemies = instance.enemies.Remove((Enemy) entity);
			enemyCount --;
			if (enemyCount == 0)
			{
				SpawnEnemies ();
				AddScore (1);
			}
		}

		void OnDestroy ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			GameManager.updatables = new IUpdatable[0];
			for (int i = 0; i < enemies.Length; i ++)
			{
				Enemy enemy = instance.enemies[i];
				enemy.onDeath -= OnEnemyKilled;
			}
		}

		[Serializable]
		public class EnemyEntry : IUpdatable
		{
			public Enemy enemyPrefab;
			public float minCreateRangeFromPlayer;
			
			public void DoUpdate ()
			{
				Vector2 spawnPosition;
				do
				{
					spawnPosition = Random.insideUnitCircle * (Area.Instance.trs.localScale.x / 2 - enemyPrefab.radius);
				} while (((Vector2) Player.Instance.trs.position - spawnPosition).sqrMagnitude < minCreateRangeFromPlayer * minCreateRangeFromPlayer);
				bool isValidSpawnPosition = true;
				for (int i = 0; i < Instance.enemies.Length; i ++)
				{
					Enemy enemy = instance.enemies[i];
					float minCreateRangeFromEnemy = enemy.radius + enemyPrefab.radius;
					if (((Vector2) enemy.trs.position - spawnPosition).sqrMagnitude < minCreateRangeFromEnemy * minCreateRangeFromEnemy)
					{
						isValidSpawnPosition = false;
						break;
					}
				}
				if (isValidSpawnPosition)
				{
					Enemy newEnemy = Instantiate(enemyPrefab, spawnPosition, Quaternion.identity);
					newEnemy.onDeath += instance.OnEnemyKilled;
					instance.enemies = instance.enemies.Add(newEnemy);
					GameManager.updatables = GameManager.updatables.Remove(this);
				}
			}
		}
	}
}
