using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	public class Shooter : UpdateWhileEnabled
	{
		[Header("Shooter")]
		public Transform trs;
		public float shootRange;
		public BulletPatternEntry[] bulletPatternEntries;
		public Dictionary<string, BulletPatternEntry> bulletPatternEntriesDict = new Dictionary<string, BulletPatternEntry>();
		public Animator animator;

		void Awake ()
		{
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry bulletPatternEntry = bulletPatternEntries[i];
				bulletPatternEntriesDict.Add(bulletPatternEntry.name, bulletPatternEntry);
			}
		}

		public override void DoUpdate ()
		{
			Vector2 toPlayer = Player.Instance.trs.position - trs.position;
			if (toPlayer.sqrMagnitude < shootRange * shootRange)
				animator.Play("Shoot");
		}

		public void Shoot (string name)
		{
			bulletPatternEntriesDict[name].Shoot ();
		}
	}
}