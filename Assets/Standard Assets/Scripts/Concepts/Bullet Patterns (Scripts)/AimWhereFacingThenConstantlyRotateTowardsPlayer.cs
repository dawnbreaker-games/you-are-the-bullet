﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	[CreateAssetMenu]
	public class AimWhereFacingThenConstantlyRotateTowardsPlayer : AimWhereFacing
	{
		[MakeConfigurable]
		public float rotateRate;
		
		public override Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			Bullet[] output = base.Shoot(spawner, bulletPrefab, positionOffset);
			for (int i = 0; i < output.Length; i ++)
			{
				Bullet bullet = output[i];
				GameManager.updatables = GameManager.updatables.Add(new RotateUpdater(bullet, this));
			}
			return output;
		}

		class RotateUpdater : IUpdatable
		{
			Bullet bullet;
			AimWhereFacingThenConstantlyRotateTowardsPlayer aimWhereFacingThenConstantlyRotateTowardsPlayer;

			public RotateUpdater (Bullet bullet, AimWhereFacingThenConstantlyRotateTowardsPlayer aimWhereFacingThenConstantlyRotateTowardsPlayer)
			{
				this.bullet = bullet;
				this.aimWhereFacingThenConstantlyRotateTowardsPlayer = aimWhereFacingThenConstantlyRotateTowardsPlayer;
			}

			public void DoUpdate ()
			{
				if (bullet == null || !bullet.gameObject.activeInHierarchy)
					GameManager.updatables = GameManager.updatables.Remove(this);
				else
					bullet.Move (bullet.rigid.velocity.normalized.RotateTo(Player.instance.trs.position - bullet.trs.position, aimWhereFacingThenConstantlyRotateTowardsPlayer.rotateRate * Time.deltaTime));
			}
		}
	}
}