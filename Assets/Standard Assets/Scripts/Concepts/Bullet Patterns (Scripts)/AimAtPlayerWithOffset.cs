﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	[CreateAssetMenu]
	public class AimAtPlayerWithOffset : AimAtPlayer
	{
		[MakeConfigurable]
		public float shootOffset;
		
		public override Vector2 GetShootDirection (Transform spawner)
		{
			return base.GetShootDirection(spawner).Rotate(shootOffset);
		}
	}
}