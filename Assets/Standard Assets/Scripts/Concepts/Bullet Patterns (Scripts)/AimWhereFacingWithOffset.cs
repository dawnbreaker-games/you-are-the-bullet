﻿using Extensions;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	[CreateAssetMenu]
	public class AimWhereFacingWithOffset : AimWhereFacing
	{
		[MakeConfigurable]
		public float offset;
		
		public override Vector2 GetShootDirection (Transform spawner)
		{
			return base.GetShootDirection(spawner).Rotate(offset);
		}
	}
}