﻿using System;
using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	public class BulletPattern : ScriptableObject, IConfigurable
	{
		public virtual string Name
		{
			get
			{
				return name;
			}
		}
		public virtual string Category
		{
			get
			{
				return "Bullet Patterns";
			}
		}

		public virtual void Init (Transform spawner)
		{			
		}

		public virtual Vector2 GetShootDirection (Transform spawner)
		{
			return spawner.up;
		}
		
		public virtual Bullet[] Shoot (Transform spawner, Bullet bulletPrefab, float positionOffset = 0)
		{
			return new Bullet[] { ObjectPool.Instance.SpawnComponent<Bullet>(bulletPrefab.prefabIndex, spawner.position + spawner.up * positionOffset, Quaternion.LookRotation(Vector3.forward, GetShootDirection(spawner))) };
		}
		
		public virtual Bullet[] Shoot (Vector2 spawnPos, Vector2 direction, Bullet bulletPrefab, float positionOffset = 0)
		{
			return new Bullet[] { ObjectPool.Instance.SpawnComponent<Bullet>(bulletPrefab.prefabIndex, spawnPos + direction.normalized * positionOffset, Quaternion.LookRotation(Vector3.forward, direction)) };
		}
		
		public virtual void Shoot (Transform spawner, Bullet bulletPrefab, Action<Bullet[]> onShot, float positionOffset = 0)
		{
			Bullet[] bullets = new Bullet[] { ObjectPool.Instance.SpawnComponent<Bullet>(bulletPrefab.prefabIndex, spawner.position + spawner.up * positionOffset, Quaternion.LookRotation(Vector3.forward, GetShootDirection(spawner))) };
			onShot (bullets);
		}
		
		public virtual void Shoot (Vector2 spawnPos, Vector2 direction, Bullet bulletPrefab, Action<Bullet[]> onShot, float positionOffset = 0)
		{
			Bullet[] bullets = new Bullet[] { ObjectPool.Instance.SpawnComponent<Bullet>(bulletPrefab.prefabIndex, spawnPos + direction.normalized * positionOffset, Quaternion.LookRotation(Vector3.forward, direction)) };
			onShot (bullets);
		}
		
		public virtual IEnumerator RetargetAfterDelay (Bullet bullet, float delay)
		{
			yield return new WaitForSeconds(delay);
			if (bullet == null || !bullet.gameObject.activeSelf)
				yield break;
			yield return Retarget (bullet);
		}
		
		public virtual IEnumerator RetargetAfterDelay (Bullet bullet, Vector2 direction, float delay)
		{
			yield return new WaitForSeconds(delay);
			if (bullet == null || !bullet.gameObject.activeSelf)
				yield break;
			yield return Retarget (bullet, direction);
		}

		public virtual Bullet Retarget (Bullet bullet)
		{
			bullet.trs.up = GetRetargetDirection(bullet);
			bullet.Move (bullet.trs.up);
			return bullet;
		}
		
		public virtual Bullet Retarget (Bullet bullet, Vector2 direction)
		{
			bullet.trs.up = direction;
			bullet.Move (bullet.trs.up);
			return bullet;
		}
		
		public virtual Vector2 GetRetargetDirection (Bullet bullet)
		{
			return bullet.trs.up;
		}
		
		public virtual IEnumerator SplitAfterDelay (Bullet bullet, Bullet splitBulletPrefab, float delay, float positionOffset = 0)
		{
			yield return new WaitForSeconds(delay);
			if (!bullet.gameObject.activeSelf)
				yield break;
			yield return Split (bullet, splitBulletPrefab, positionOffset);
		}
		
		public virtual IEnumerator SplitAfterDelay (Bullet bullet, Vector2 direction, Bullet splitBulletPrefab, float delay, float positionOffset = 0)
		{
			yield return new WaitForSeconds(delay);
			if (!bullet.gameObject.activeSelf)
				yield break;
			yield return Split (bullet, direction, splitBulletPrefab, positionOffset);
		}

		public virtual Bullet[] Split (Bullet bullet, Bullet splitBulletPrefab, float positionOffset = 0)
		{
			return Shoot (bullet.trs.position, GetSplitDirection(bullet), splitBulletPrefab, positionOffset);
		}

		public virtual Bullet[] Split (Bullet bullet, Vector2 direction, Bullet splitBulletPrefab, float positionOffset = 0)
		{
			return Shoot (bullet.trs.position, direction, splitBulletPrefab, positionOffset);
		}
		
		public virtual Vector2 GetSplitDirection (Bullet bullet)
		{
			return bullet.trs.up;
		}
	}
}