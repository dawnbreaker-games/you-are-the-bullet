﻿﻿using System;
using Extensions;
using UnityEngine;
using System.Collections;

namespace YouAreTheBullet
{
	[CreateAssetMenu]
	public class RepeatBulletPatternsWithDelay : BulletPattern
	{
		// [MakeConfigurable]
		public int repeatCount;
		public BulletPatternEntry[] bulletPatternEntries;

		public override void Init (Transform spawner)
		{
			for (int i = 0; i < bulletPatternEntries.Length; i ++)
			{
				BulletPatternEntry BulletPatternEntry = bulletPatternEntries[i];
				BulletPatternEntry.bulletPattern.Init (spawner);
			}
		}

		public override void Shoot (Transform spawner, Bullet bulletPrefab, Action<Bullet[]> onShot, float positionOffset = 0)
		{
			GameManager.updatables = GameManager.updatables.Add(new ShootUpdater(this, spawner, bulletPrefab, onShot, positionOffset));
		}
		
		public override void Shoot (Vector2 spawnPosition, Vector2 direction, Bullet bulletPrefab, Action<Bullet[]> onShot, float positionOffset = 0)
		{
			GameManager.updatables = GameManager.updatables.Add(new ShootUpdater(this, spawnPosition, direction, bulletPrefab, onShot, positionOffset));
		}

		class ShootUpdater : IUpdatable
		{
			RepeatBulletPatternsWithDelay repeatBulletPatternsWithDelay;
			Action<Bullet[]> onShot;
			Transform spawner;
			Vector2 spawnPosition;
			Vector2 direction;
			Bullet bulletPrefab;
			float positionOffset;
			float delayTimer;
			int currentBulletPatternIndex;
			int repeatIndex;

			public ShootUpdater (RepeatBulletPatternsWithDelay repeatBulletPatternsWithDelay, Transform spawner, Bullet bulletPrefab, Action<Bullet[]> onShot, float positionOffset = 0)
			{
				this.repeatBulletPatternsWithDelay = repeatBulletPatternsWithDelay;
				this.spawner = spawner;
				this.bulletPrefab = bulletPrefab;
				this.onShot = onShot;
				this.positionOffset = positionOffset;
			}

			public ShootUpdater (RepeatBulletPatternsWithDelay repeatBulletPatternsWithDelay, Vector2 spawnPosition, Vector2 direction, Bullet bulletPrefab, Action<Bullet[]> onShot, float positionOffset = 0)
			{
				this.repeatBulletPatternsWithDelay = repeatBulletPatternsWithDelay;
				this.spawnPosition = spawnPosition;
				this.direction = direction;
				this.bulletPrefab = bulletPrefab;
				this.onShot = onShot;
				this.positionOffset = positionOffset;
			}

			public void DoUpdate ()
			{
				delayTimer -= Time.deltaTime;
				if (delayTimer <= 0)
				{
					BulletPatternEntry bulletPatternEntry = repeatBulletPatternsWithDelay.bulletPatternEntries[currentBulletPatternIndex];
					if (spawner != null)
						bulletPatternEntry.bulletPattern.Shoot (spawner, bulletPrefab, onShot, positionOffset);
					else
						bulletPatternEntry.bulletPattern.Shoot (spawnPosition, direction, bulletPrefab, onShot, positionOffset);
					currentBulletPatternIndex ++;
					if (currentBulletPatternIndex >= repeatBulletPatternsWithDelay.bulletPatternEntries.Length)
					{
						currentBulletPatternIndex = 0;
						repeatIndex ++;
						if (repeatIndex >= repeatBulletPatternsWithDelay.repeatCount)
							GameManager.updatables = GameManager.updatables.Remove(this);
					}
					delayTimer = bulletPatternEntry.delayNextBulletPattern;
				}
			}
		}

		[Serializable]
		public class BulletPatternEntry
		{
			public BulletPattern bulletPattern;
			public float delayNextBulletPattern;
		}
	}
}
