﻿using Extensions;
using UnityEngine;
using System.Collections;
using UnityEngine.Tilemaps;
using UnityEngine.InputSystem;
using System.Collections.Generic;
#if UNITY_EDITOR
using UnityEditor;
#endif
// using DialogAndStory;

namespace YouAreTheBullet
{
	[ExecuteInEditMode]
	public class WorldMap : SingletonUpdateWhileEnabled<WorldMap>
	{
		public Tilemap unexploredTilemap;
		Vector2Int cellPositions;
		public static HashSet<Vector2Int> exploredCellPositions = new HashSet<Vector2Int>();
		public static Vector2Int[] ExploredCellPositions
		{
			get
			{
				return SaveAndLoadManager.GetVector2IntArray("Explored cells", new Vector2Int[0]);
			}
			set
			{
				SaveAndLoadManager.SetVector2IntArray ("Explored cells", value);
			}
		}
		HashSet<Vector2Int> exploredCellPositionsAtLastTimeOpened = new HashSet<Vector2Int>();
		Vector2Int cellPosition;
		Vector2Int minCellPosition;
		Vector2Int maxCellPosition;
		Vector2Int previousMinCellPosition;
		public static WorldMapIcon[] worldMapIcons = new WorldMapIcon[0];
		public static bool isOpen;
		bool canControlCamera;
		public float cameraMoveSpeed;
		Vector2 moveInput;
		public float normalizedScreenBorder;
		Rect screenWithoutBorder;
		SavePoint fastTravelToSavePoint;
		public static bool playerIsAtSavePoint;
		public static bool playerJustFastTraveled;

		public override void Awake ()
		{
			base.Awake ();
			minCellPosition = unexploredTilemap.WorldToCell(GameCamera.Instance.viewRect.min).ToVec2Int();
			maxCellPosition = unexploredTilemap.WorldToCell(GameCamera.instance.viewRect.max).ToVec2Int();
			exploredCellPositions.Clear();
			foreach (Vector2Int cellPosition in ExploredCellPositions)
				exploredCellPositions.Add(cellPosition);
			previousMinCellPosition = minCellPosition;
			screenWithoutBorder = new Rect();
			screenWithoutBorder.size = new Vector2(Screen.width - normalizedScreenBorder * Screen.width, Screen.height - normalizedScreenBorder * Screen.height);
			screenWithoutBorder.center = new Vector2(Screen.width / 2, Screen.height / 2);
			WorldMapCamera.Instance.HandleViewSize ();
		}
		
		public override void DoUpdate ()
		{
			if (!isOpen)
				UpdateExplored ();
			else
			{
				WorldMapCamera.instance.DoUpdate ();
				moveInput = InputManager.GetMoveInput() + InputManager.GetAimInput();
				// GameManager.activeCursorEntry.rectTrs.gameObject.SetActive(true);
				// if (InputManager.UsingGamepad)
				// {
				// 	GameManager.activeCursorEntry.rectTrs.position += (Vector3) moveInput * GameManager.cursorMoveSpeed * Time.unscaledDeltaTime;
				// 	GameManager.activeCursorEntry.rectTrs.position = GameManager.activeCursorEntry.rectTrs.position.ClampComponents(Vector3.zero, new Vector2(Screen.width, Screen.height));
				// }
				if (!screenWithoutBorder.Contains(Mouse.current.position.ReadValue()))
				{
					moveInput = Mouse.current.position.ReadValue() - new Vector2(Screen.width / 2, Screen.height / 2);
					moveInput /= new Vector2(Screen.width / 2, Screen.height / 2).magnitude;
					if (canControlCamera)
						WorldMapCamera.instance.trs.position += (Vector3) moveInput * cameraMoveSpeed * Time.unscaledDeltaTime;
					// if (GameManager.activeCursorEntry.name != "Arrow")
					// {
					// 	GameManager.cursorEntriesDict["Arrow"].SetAsActive ();
					// 	GameManager.activeCursorEntry.rectTrs.position = GameManager.cursorEntriesDict["Default"].rectTrs.position;
					// }
					// GameManager.activeCursorEntry.rectTrs.up = moveInput;
					// if (GameManager.Instance.worldMapTutorialConversation.currentDialog == GameManager.Instance.worldMapMoveViewTutorialDialog)
					// 	DialogManager.Instance.EndDialog (GameManager.Instance.worldMapMoveViewTutorialDialog);
				}
				// else if (GameManager.activeCursorEntry.name != "Default")
				// {
				// 	GameManager.cursorEntriesDict["Default"].SetAsActive ();
				// 	GameManager.activeCursorEntry.rectTrs.position = GameManager.cursorEntriesDict["Arrow"].rectTrs.position;
				// }
				if (playerIsAtSavePoint)
					HandleFastTravel ();
			}
		}

		void HandleFastTravel ()
		{
			foreach (SavePoint savePoint in SavePoint.instances)
			{
				if (savePoint.Found)
				{
					if (fastTravelToSavePoint != savePoint && savePoint.worldMapIcon.collider.bounds.ToRect().Contains(WorldMapCamera.instance.camera.ScreenToWorldPoint(Mouse.current.position.ReadValue())))
					{
						if (fastTravelToSavePoint != null)
							fastTravelToSavePoint.worldMapIcon.Unhighlight ();
						fastTravelToSavePoint = savePoint;
						fastTravelToSavePoint.worldMapIcon.Highlight ();
						break;
					}
				}
			}
			if (fastTravelToSavePoint != null && InputManager.GetInteractInput())
			{
				playerJustFastTraveled = true;
				Player.StartPosition = fastTravelToSavePoint.worldMapIcon.collider.bounds.center;
				Close ();
				isOpen = false;
				GameManager.Instance.ReloadActiveScene ();
			}
		}

		IEnumerator OpenRoutine ()
		{
			canControlCamera = false;
			// GameManager.Instance.PauseGame (1000000);
			foreach (WorldMapIcon worldMapIcon in worldMapIcons)
			{
				foreach (Vector2Int position in worldMapIcon.cellBoundsRect.allPositionsWithin)
				{
					if (!worldMapIcon.onlyMakeIfExplored || exploredCellPositions.Contains(position))
						worldMapIcon.MakeIcon ();
				}
			}
			HashSet<Vector3Int> exploredCellPositionsSinceLastTimeOpened = new HashSet<Vector3Int>();
			foreach (Vector2Int exploredCellPosition in exploredCellPositions)
				exploredCellPositionsSinceLastTimeOpened.Add(exploredCellPosition.ToVec3Int());
			foreach (Vector2Int exploredCellPositionAtLastTimeOpened in exploredCellPositionsAtLastTimeOpened)
				exploredCellPositionsSinceLastTimeOpened.Remove(exploredCellPositionAtLastTimeOpened.ToVec3Int());
			Vector3Int[] _exploredCellPositionsSinceLastTimeOpened = new Vector3Int[exploredCellPositionsSinceLastTimeOpened.Count];
			exploredCellPositionsSinceLastTimeOpened.CopyTo(_exploredCellPositionsSinceLastTimeOpened);
			unexploredTilemap.SetTiles(_exploredCellPositionsSinceLastTimeOpened, new TileBase[exploredCellPositionsSinceLastTimeOpened.Count]);
			WorldMapCamera.instance.trs.position = Player.Instance.trs.position.SetZ(WorldMapCamera.instance.trs.position.z);
			WorldMapCamera.instance.gameObject.SetActive(true);
			// if (GameManager.Instance.worldMapTutorialConversation.gameObject.activeSelf && GameManager.Instance.worldMapTutorialConversation.updateRoutine == null)
			// {
			// 	DialogManager.Instance.StartConversation (GameManager.Instance.worldMapTutorialConversation);
			// 	foreach (Dialog dialog in GameManager.Instance.worldMapTutorialConversation.dialogs)
			// 		dialog.canvas.worldCamera = WorldMapCamera.instance.camera;
			// }
			// if (InputManager.UsingGamepad)
			// {
			// 	GameManager.cursorEntriesDict["Default"].SetAsActive ();
			// 	GameManager.activeCursorEntry.rectTrs.localPosition = Vector2.zero;
			// }
			yield return new WaitForEndOfFrame();
			yield return new WaitForEndOfFrame();
			canControlCamera = true;
		}

		public virtual void Open ()
		{
			if (Instance != this)
			{
				instance.Open ();
				return;
			}
			isOpen = true;
			StopAllCoroutines();
			StartCoroutine(OpenRoutine ());
		}

		public virtual void Close ()
		{
			if (Instance != this)
			{
				instance.Close ();
				return;
			}
			isOpen = false;
			exploredCellPositionsAtLastTimeOpened.Clear();
			foreach (Vector2Int exploredCellPosition in exploredCellPositions)
				exploredCellPositionsAtLastTimeOpened.Add(exploredCellPosition);
			foreach (WorldMapIcon worldMapIcon in worldMapIcons)
			{
				worldMapIcon.DestroyIcon ();
				worldMapIcon.Unhighlight ();
			}
			WorldMapCamera.instance.gameObject.SetActive(false);
			// if (!PauseMenu.Instance.gameObject.activeSelf)
				// GameManager.Instance.PauseGame (-1000000);
			// if (!InputManager.UsingGamepad)
			// 	GameManager.cursorEntriesDict["Default"].SetAsActive ();
			// else
			// 	GameManager.activeCursorEntry.rectTrs.gameObject.SetActive(false);
		}
		
		void UpdateExplored ()
		{
			int x;
			int y;
			minCellPosition = unexploredTilemap.WorldToCell(GameCamera.Instance.viewRect.min).ToVec2Int();
			maxCellPosition = unexploredTilemap.WorldToCell(GameCamera.Instance.viewRect.max).ToVec2Int();
			// if (justTeleported)
			// {
			// 	justTeleported = false;
			// 	minExploredCellPosition = VectorExtensions.SetToMinComponents(minExploredCellPosition, minCellPosition);
			// 	maxExploredCellPosition = VectorExtensions.SetToMaxComponents(maxExploredCellPosition, maxCellPosition);
			// 	for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
			// 	{
			// 		for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
			// 		{
			// 			cellPosition = new Vector2Int(x, y);
			// 			exploredCellPositions.Add(cellPosition);
			// 		}
			// 	}
			// 	previousMinCellPosition = minCellPosition;
			// 	return;
			// }
			if (minCellPosition.x > previousMinCellPosition.x)
			{
				x = maxCellPosition.x;
				for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
				{
					cellPosition = new Vector2Int(x, y);
					exploredCellPositions.Add(cellPosition);
				}
				if (minCellPosition.y > previousMinCellPosition.y)
				{
					y = maxCellPosition.y;
					for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
					{
						cellPosition = new Vector2Int(x, y);
						exploredCellPositions.Add(cellPosition);
					}
				}
				else if (minCellPosition.y < previousMinCellPosition.y)
				{
					y = minCellPosition.y;
					for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
					{
						cellPosition = new Vector2Int(x, y);
						exploredCellPositions.Add(cellPosition);
					}
				}
			}
			else if (minCellPosition.x < previousMinCellPosition.x)
			{
				x = minCellPosition.x;
				for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
				{
					cellPosition = new Vector2Int(x, y);
					exploredCellPositions.Add(cellPosition);
				}
				if (minCellPosition.y > previousMinCellPosition.y)
				{
					y = maxCellPosition.y;
					for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
					{
						cellPosition = new Vector2Int(x, y);
						exploredCellPositions.Add(cellPosition);
					}
				}
				else if (minCellPosition.y < previousMinCellPosition.y)
				{
					y = minCellPosition.y;
					for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
					{
						cellPosition = new Vector2Int(x, y);
						exploredCellPositions.Add(cellPosition);
					}
				}
			}
			else if (minCellPosition.y > previousMinCellPosition.y)
			{
				y = maxCellPosition.y;
				for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
				{
					cellPosition = new Vector2Int(x, y);
					exploredCellPositions.Add(cellPosition);
				}
				if (minCellPosition.x > previousMinCellPosition.x)
				{
					x = maxCellPosition.x;
					for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
					{
						cellPosition = new Vector2Int(x, y);
						exploredCellPositions.Add(cellPosition);
					}
				}
				else if (minCellPosition.x < previousMinCellPosition.x)
				{
					x = minCellPosition.x;
					for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
					{
						cellPosition = new Vector2Int(x, y);
						exploredCellPositions.Add(cellPosition);
					}
				}
			}
			else if (minCellPosition.y < previousMinCellPosition.y)
			{
				y = minCellPosition.y;
				for (x = minCellPosition.x; x <= maxCellPosition.x; x ++)
				{
					cellPosition = new Vector2Int(x, y);
					exploredCellPositions.Add(cellPosition);
				}
				if (minCellPosition.x > previousMinCellPosition.x)
				{
					x = maxCellPosition.x;
					for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
					{
						cellPosition = new Vector2Int(x, y);
						exploredCellPositions.Add(cellPosition);
					}
				}
				else if (minCellPosition.x < previousMinCellPosition.x)
				{
					x = minCellPosition.x;
					for (y = minCellPosition.y; y <= maxCellPosition.y; y ++)
					{
						cellPosition = new Vector2Int(x, y);
						exploredCellPositions.Add(cellPosition);
					}
				}
			}
			previousMinCellPosition = minCellPosition;
		}
	}
}