﻿using Extensions;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;
using System.Collections;
using UnityEngine.InputSystem;

namespace YouAreTheBullet
{
	//[ExecuteAlways]
	public class Hotkey : MonoBehaviour
	{
		public string inputBinding;
		public Button button;
		public UnityEvent unityEvent;
		public InputAction anyButtonAction;

		void OnEnable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			if (!string.IsNullOrEmpty(inputBinding))
				anyButtonAction = new InputAction(binding: inputBinding);
			anyButtonAction.performed += Do;
			anyButtonAction.Enable();
		}

		void OnDisable ()
		{
#if UNITY_EDITOR
			if (!Application.isPlaying)
				return;
#endif
			anyButtonAction.Disable();
			anyButtonAction.performed -= Do;
		}

		void Do (InputAction.CallbackContext context)
		{
			unityEvent.Invoke();
		}
	}
}