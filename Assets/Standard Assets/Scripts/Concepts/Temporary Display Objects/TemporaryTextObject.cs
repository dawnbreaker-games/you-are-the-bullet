using TMPro;
using System;
using System.Collections;

namespace YouAreTheBullet
{
	[Serializable]
	public class TemporaryTextObject : TemporaryDisplayObject
	{
		public TMP_Text text;
		public float durationPerCharacter;
		
		public override IEnumerator DisplayRoutine ()
		{
			duration = text.text.Length * durationPerCharacter;
			yield return base.DisplayRoutine ();
		}
	}
}