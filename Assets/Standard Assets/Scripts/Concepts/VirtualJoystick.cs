using System;
using Extensions;
using UnityEngine;
using UnityEngine.UI;
using System.Collections;
using UnityEngine.InputSystem;
using System.Collections.Generic;
using UnityEngine.InputSystem.Controls;

namespace YouAreTheBullet
{
	public class VirtualJoystick : UpdateWhileEnabled
	{
		public RectTransform rectTrs;
		public RectTransform canvasRectTrs;
		[HideInInspector]
		public Circle2D outerCircle;
		[HideInInspector]
		public Vector2 touchPosition;
		bool isUsing;
		TouchControl touchControl;

		void Start ()
		{
			RectTransform parent = (RectTransform) rectTrs.parent;
			Rect rect = parent.GetWorldRect();
			outerCircle = new Circle2D(rect.center, rect.width / 2);
		}

		public override void DoUpdate ()
		{
			if (touchControl == null)
			{
				for (int i = 0; i < Touchscreen.current.touches.Count; i ++)
				{
					TouchControl touchControl = Touchscreen.current.touches[i];
					Vector2 touchPosition = touchControl.position.ReadValue();
					if (outerCircle.Contains(touchPosition))
					{
						this.touchControl = touchControl;
						this.touchPosition = touchPosition;
						isUsing = true;
						break;
					}
				}
			}
			else
			{
				if (touchControl.isInProgress)
					touchPosition = touchControl.position.ReadValue();
				else
				{
					isUsing = false;
					touchControl = null;
				}
			}
			if (isUsing)
				rectTrs.position = outerCircle.ClosestPoint(touchPosition);
		}
	}
}