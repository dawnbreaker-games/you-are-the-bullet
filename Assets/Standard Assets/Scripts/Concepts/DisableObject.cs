﻿using UnityEngine;
using System.Collections;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	public class DisableObject : MonoBehaviour
	{
		void Awake ()
		{
			if (!enabled)
				return;
			gameObject.SetActive(false);
		}
	}
}