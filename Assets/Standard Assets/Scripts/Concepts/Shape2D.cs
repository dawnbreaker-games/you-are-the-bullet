using System;
using Extensions;
using UnityEngine;
using System.Collections.Generic;
using Random = UnityEngine.Random;

[Serializable]
public class Shape2D
{
	public Vector2[] corners;
	public LineSegment2D[] edges;

	public Shape2D ()
	{
	}

	public Shape2D (params Vector2[] corners)
	{
		this.corners = corners;
		SetEdges ();
	}

	public Shape2D (params LineSegment2D[] edges)
	{
		this.edges = edges;
		SetCorners ();
	}

#if UNITY_EDITOR
	public virtual void DrawGizmos (Color color)
	{
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			edge.DrawGizmos (color);
		}
	}
#endif

	public virtual void SetCorners ()
	{
		corners = new Vector2[edges.Length];
		for (int i = 0; i < edges.Length; i ++)
			corners[i] = edges[i].end;
	}

	public virtual void SetEdges ()
	{
		edges = new LineSegment2D[corners.Length];
		Vector3 previousCorner = corners[corners.Length - 1];
		for (int i = 0; i < corners.Length; i ++)
		{
			Vector2 corner = corners[i];
			edges[i] = new LineSegment2D(previousCorner, corner);
			previousCorner = corner;
		}
	}

	public virtual float GetPerimeter ()
	{
		float output = 0;
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			output += edge.GetLength();
		}
		return output;
	}

	public virtual Vector2 GetPointOnPerimeter (float distance)
	{
		float perimeter = GetPerimeter();
		while (true)
		{
			for (int i = 0; i < edges.Length; i ++)
			{
				LineSegment2D edge = edges[i];
				float edgeLength = edge.GetLength();
				distance -= edgeLength;
				if (distance <= 0)
					return edge.GetPointWithDirectedDistance(edgeLength + distance);
			}
		}
	}

	public virtual bool Contains_Polygon (Vector2 point, bool equalPointsIntersect = true, float checkDistance = 99999)
	{
		LineSegment2D checkLineSegment = new LineSegment2D(point, point + (Random.insideUnitCircle.normalized * checkDistance));
		int collisionCount = 0;
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			if (edge.DoIIntersectWithLineSegment(checkLineSegment, equalPointsIntersect))
				collisionCount ++;
		}
		return collisionCount % 2 == 1;
	}

	public virtual bool IsPolygon ()
	{
		throw new NotImplementedException();
	}

	public virtual bool DoIIntersectWithLineSegment (LineSegment2D lineSegment, bool equalPointsIntersect = true)
	{
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			if (edge.DoIIntersectWithLineSegment(lineSegment, equalPointsIntersect))
				return true;
		}
		return false;
	}

	public virtual Vector2 GetRandomPoint (bool checkIfContained = true, bool containsEdges = true, float checkDistance = 99999)
	{
		float perimeter = GetPerimeter();
		while (true)
		{
			Vector2 point1 = GetPointOnPerimeter(Random.Range(0, perimeter));
			Vector2 point2 = GetPointOnPerimeter(Random.Range(0, perimeter));
			Vector2 output = (point1 + point2) / 2;
			if (!checkIfContained || Contains_Polygon(output, containsEdges, checkDistance))
				return output;
		}
	}

	public Vector2 GetClosestPoint (Vector2 point, bool pointsInsideHave0Distance = false, float checkDistance = 99999)
	{
		(Vector2 point, float distanceSqr) closestPointAndDistanceSqr = GetClosestPointAndDistanceSqr(point, pointsInsideHave0Distance, checkDistance);
		return closestPointAndDistanceSqr.point;
	}

	public float GetDistanceSqr (Vector2 point, bool pointsInsideHave0Distance = false, float checkDistance = 99999)
	{
		(Vector2 point, float distanceSqr) closestPointAndDistanceSqr = GetClosestPointAndDistanceSqr(point, pointsInsideHave0Distance, checkDistance);
		return closestPointAndDistanceSqr.distanceSqr;
	}

	public (Vector2, float) GetClosestPointAndDistanceSqr (Vector2 point, bool pointsInsideHave0Distance = false, float checkDistance = 99999)
	{
		if (pointsInsideHave0Distance && Contains_Polygon(point, checkDistance: checkDistance))
			return (point, 0);
		else
		{
			Vector2 closestPoint = new Vector2();
			float closestDistanceSqr = Mathf.Infinity;
			float distanceSqr = 0;
			for (int i = 0; i < edges.Length; i ++)
			{
				LineSegment2D edge = edges[i];
				Vector2 pointOnPerimeter = edge.GetClosestPoint(point);
				distanceSqr = (point - pointOnPerimeter).sqrMagnitude;
				if (distanceSqr < closestDistanceSqr)
				{
					closestDistanceSqr = distanceSqr;
					closestPoint = pointOnPerimeter;
				}
			}
			return (closestPoint, closestDistanceSqr);
		}
	}

	public float GetDistanceSqr (Shape2D shape, bool containsEdges = true, bool pointsInsideHave0Distance = false, float checkDistance = 99999)
	{
		if (DoIIntersectWithShape(shape, containsEdges))
			return 0;
		else
		{
			if (pointsInsideHave0Distance && (shape.Contains_Polygon(corners[0], containsEdges, checkDistance) || Contains_Polygon(shape.corners[0], containsEdges, checkDistance)))
				return 0;
			Vector2 closestPoint = new Vector2();
			float closestDistanceSqr = Mathf.Infinity;
			float distanceSqr = 0;
			for (int i = 0; i < corners.Length; i ++)
			{
				Vector2 corner = corners[i];
				Vector2 pointOnPerimeter = shape.GetClosestPoint(corner, pointsInsideHave0Distance, checkDistance);
				distanceSqr = (corner - pointOnPerimeter).sqrMagnitude;
				if (distanceSqr < closestDistanceSqr)
				{
					closestDistanceSqr = distanceSqr;
					if (closestDistanceSqr == 0)
						return 0;
					closestPoint = pointOnPerimeter;
				}
			}
			for (int i = 0; i < shape.corners.Length; i ++)
			{
				Vector2 corner = shape.corners[i];
				Vector2 pointOnPerimeter = GetClosestPoint(corner, pointsInsideHave0Distance, checkDistance);
				distanceSqr = (corner - pointOnPerimeter).sqrMagnitude;
				if (distanceSqr < closestDistanceSqr)
				{
					closestDistanceSqr = distanceSqr;
					if (closestDistanceSqr == 0)
						return 0;
					closestPoint = pointOnPerimeter;
				}
			}
			return closestDistanceSqr;
		}
	}

	public bool DoIIntersectWithShape (Shape2D shape, bool equalPointsIntersect = true)
	{
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			if (shape.DoIIntersectWithLineSegment(edge))
				return true;
		}
		return false;
	}

	public Shape2D Subdivide ()
	{
		List<LineSegment2D> output = new List<LineSegment2D>();
		for (int i = 0; i < edges.Length; i ++)
		{
			LineSegment2D edge = edges[i];
			output.Add(new LineSegment2D(edge.start, edge.GetMidpoint()));
			output.Add(new LineSegment2D(edge.GetMidpoint(), edge.end));
		}
		return new Shape2D(output.ToArray());
	}

	public Rect GetBoundsRect ()
	{
		Vector2 min = corners[0];
		Vector2 max = min;
		for (int i = 1; i < corners.Length; i ++)
		{
			Vector2 corner = corners[i];
			min = min.SetToMinComponents(corner);
			max = max.SetToMaxComponents(corner);
		}
		return Rect.MinMaxRect(min.x, min.y, max.x, max.y);
	}

	public Vector2[] GetOverlappingCellPositions_Polygon (float gridCellSize, bool equalPointsIntersect = true, float checkDistance = 99999)
	{
		List<Vector2> output = new List<Vector2>();
		Rect boundsRect = GetBoundsRect();
		for (float x = boundsRect.xMin; x <= boundsRect.xMax; x += gridCellSize)
		{
			for (float y = boundsRect.yMin; y <= boundsRect.yMax; y += gridCellSize)
			{
				Vector2 position = new Vector2(x, y);
				if (Contains_Polygon(position, equalPointsIntersect, checkDistance))
					output.Add(position);
			}
		}
		return output.ToArray();
	}

	public Shape2D Move (Vector2 move)
	{
		Vector2[] outputCorners = new Vector2[corners.Length];
		for (int i = 0; i < corners.Length; i ++)
			outputCorners[i] = corners[i] + move;
		return new Shape2D(outputCorners);
	}

	public Shape2D Grow (Vector2 pushAwayFrom, float amount)
	{
		Vector2[] outputCorners = new Vector2[corners.Length];
		for (int i = 0; i < corners.Length; i ++)
		{
			Vector2 corner = corners[i];
			Vector2 push = corner - pushAwayFrom;
			outputCorners[i] = pushAwayFrom + push.normalized * (push.magnitude + amount);
		}
		return new Shape2D(outputCorners);
	}

	public Circle2D[] GetFillResult (FloatRange radiusRange, float growInterval, float gridCellSize, bool equalPointsIntersect = true, float checkDistance = 99999)
	{
		throw new NotImplementedException();
	}

	public Shape2D Combine (Shape2D shape)
	{
		throw new NotImplementedException();
	}

	public Shape2D Intersection_Polygon (Shape2D shape)
	{
		throw new NotImplementedException();
	}

	public Shape2D Boolean_Polygon (Shape2D outputCanOnlyBeInsideMe)
	{
		throw new NotImplementedException();
	}

	public Shape2D[] GetFillResult (params Shape2D[] shapes)
	{
		throw new NotImplementedException();
	}
}