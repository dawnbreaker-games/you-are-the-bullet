﻿#if UNITY_EDITOR
using Extensions;
using UnityEngine;
using UnityEditor;
using UnityEngine.U2D;
using System.Collections.Generic;

namespace YouAreTheBullet
{
	//[ExecuteAlways]
	public class SetRandomSpriteShapeRendererColor : EditorScript
	{
		public SpriteShapeRenderer spriteShapeRenderer;

		public override void Do ()
		{
			if (spriteShapeRenderer == null)
				spriteShapeRenderer = GetComponent<SpriteShapeRenderer>();
			Do (spriteShapeRenderer);
		}

		static void Do (SpriteShapeRenderer spriteShapeRenderer)
		{
			spriteShapeRenderer.color = ColorExtensions.RandomColor();
		}

		[MenuItem("Tools/Set selected SpriteShapeRenderers colors")]
		static void DoToSelected ()
		{
			SpriteShapeRenderer[] spriteShapeRenderers = SelectionExtensions.GetSelected<SpriteShapeRenderer>();
			for (int i = 0; i < spriteShapeRenderers.Length; i ++)
			{
				SpriteShapeRenderer spriteShapeRenderer = spriteShapeRenderers[i];
				Do (spriteShapeRenderer);
			}
		}
	}
}
#endif